export const environment = {
  production: true,
  REST_API_BASE_PATH: 'https://www.renthunter.in/rhntr/api',
  IMAGE_RESOURCE_URL: 'https://www.renthunter.in/rhntr/api/downloadFile/'
};