
$(document).ready(function () {
	//Initialize tooltips
	$('[data-toggle="tooltip"]').tooltip();  
	$(".main-menu li a").click(function() {
		$(".main-menu li").removeClass('active');
		  $(this).parent().addClass('active');
	  });
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}

$(document).ready(function () {

	$('.navbar-toggle').click(function () {
		// $('.navbar-toggle').toggleClass('navbar-on');
		//  $('.header-fullwidth').toggleClass('opened');
		   $('#nav-hold').toggleClass('nav-opened');
		 //  $('body').toggleClass('body-scroll');
		   $('.overl').toggleClass('over-on');
	   });
	   $( "#accordion" ).accordion({
		heightStyle: "content",
		active:false,
		collapsible: true,
		header:"div.accordianheader"
	  });
	
	var wow = new WOW({ mobile: false });
	wow.init();
	$('.lightbox').nivoLightbox({ effect: 'fadeScale', keyboardNav: true, });
	$('.counterUp').counterUp({ delay: 10, time: 1000 });

	$('.mobile-menu').slicknav({
		prependTo: '.navbar-header',
		parentTag: 'liner',
		allowParentLinks: true,
		duplicate: true,
		label: '',
		closedSymbol: '<i class="lni-chevron-right"></i>',
		openedSymbol: '<i class="lni-chevron-down"></i>',
	});

	var owl; $(window).on('load', function () {
		owl = $("#owl-demo");
		owl.owlCarousel({
			navigation: false,
			slideSpeed: 300,
			paginationSpeed: 400,
			singleItem: true,
			afterInit: afterOWLinit,
			afterUpdate: afterOWLinit
		});
		function afterOWLinit() {
			$('.owl-controls .owl-page').append('<a class="item-link" />');
			var pafinatorsLink = $('.owl-controls .item-link');
			$.each(this.owl.userItems, function (i) { $(pafinatorsLink[i]).css({ 'background': 'url(' + $(this).find('img').attr('src') + ') center center no-repeat', '-webkit-background-size': 'cover', '-moz-background-size': 'cover', '-o-background-size': 'cover', 'background-size': 'cover' }).on('click', function () { owl.trigger('owl.goTo', i); }); });
			$('.owl-pagination').prepend('<a href="#prev" class="prev-owl"/>');
			$('.owl-pagination').append('<a href="#next" class="next-owl"/>');
			$(".next-owl").on('click', function () { owl.trigger('owl.next'); });
			$(".prev-owl").on('click', function () { owl.trigger('owl.prev'); });
		}
	});
	var itemList = $('.listing-container'); var gridStyle = $('.grid');
	var listStyle = $('.list'); $('.list,switchToGrid').on('click', function (e) {
		e.preventDefault();
		itemList.addClass("list-layout"); itemList.removeClass("grid-layout");
		gridStyle.removeClass("active"); listStyle.addClass("active");
	});
	gridStyle.on('click', function (e) {
		e.preventDefault(); listStyle.removeClass("active"); $(this).addClass("active");
		itemList.addClass("grid-layout"); itemList.removeClass("list-layout");
	});
	$('[data-toggle="sticky-onscroll"]').each(function () {
		var sticky = $(this);
		var stickyWrapper = $('<div>').addClass('sticky-wrapper');
		sticky.before(stickyWrapper);
		sticky.addClass('sticky');

		$(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function () {
			stickyToggle(sticky, stickyWrapper, $(this));
		});
		stickyToggle(sticky, stickyWrapper, $(window));
	});

	var stickyToggle = function (sticky, stickyWrapper, scrollElement) {
		var stickyHeight = sticky.outerHeight();
		var stickyTop = stickyWrapper.offset().top;
		if (scrollElement.scrollTop() >= stickyTop) {
			stickyWrapper.height(stickyHeight); sticky.addClass("is-sticky");
		} else {
			sticky.removeClass("is-sticky");
			stickyWrapper.height('auto');
		}
	};// SideNav Button Initialization
	$(".button-collapse").sideNav();
	// SideNav Scrollbar Initialization
	var sideNavScrollbar = document.querySelector('.custom-scrollbar');
	Ps.initialize(sideNavScrollbar);
});
(function ($) {
	"use strict"; $(window).on('load', function () {
		$('#preloader').fadeOut();
		$('#portfolio').mixItUp();
		
		var offset = 200; var duration = 500; $(window).scroll(function () { if ($(this).scrollTop() > offset) { $('.back-to-top').fadeIn(400); } else { $('.back-to-top').fadeOut(400); } });
    $('.back-to-top').on('click', function (event) { event.preventDefault(); $('html, body').animate({ scrollTop: 0 }, 600); return false; }); 
    //$.stellar({ horizontalScrolling: false, verticalOffset: 40 });
	
	})
		;




		var owl = $("#latest-property");
	owl.owlCarousel({
		navigation: false,
		pagination: false,
		slideSpeed: 1000,
		loop: true,
		stopOnHover: true,
		autoPlay: true,
		items: 3,
		itemsDesktopSmall: [1024, 2],
		itemsTablet: [600, 1],
		itemsMobile: [479, 1]
	}
	);
	var owl = $("#property-galary");
	owl.owlCarousel({
		navigation: true,
		pagination: false,
		slideSpeed: 500,
		loop: true,
		stopOnHover: true,
		autoPlay: true,
		items: 1,
	}
	);
	var owl = $("#testimonials");
	owl.owlCarousel({
		loop: true,
		navigation: false,
		pagination: false,
		slideSpeed: 1000,
		stopOnHover: true,
		autoPlay: true,
		items: 3,
		itemsDesktop: [1199, 3],
		itemsDesktopSmall: [980, 2],
		itemsTablet: [768, 1],
		itemsTablet: [767, 1],
		itemsTabletSmall: [480, 1],
		itemsMobile: [479, 1]
	});
	var owl = $("#recent-property");
	owl.owlCarousel({
		loop: true,
		navigation: false,
		pagination: false,
		slideSpeed: 1000,
		stopOnHover: true,
		autoPlay: true,
		items: 4,
		itemsDesktop: [1199, 4],
		itemsDesktopSmall: [980, 3],
		itemsTablet: [768, 2],
		itemsTablet: [767, 2],
		itemsTabletSmall: [480, 1],
		itemsMobile: [479, 1]
	});
	var owl = $("#property-slider");
	owl.owlCarousel({
		loop: true,
		navigation: true,
		pagination: false,
		slideSpeed: 1000,
		stopOnHover: true,
		autoPlay: true,
		items: 1,
	});
	var owl = $("#listing-carousel");
	owl.owlCarousel({
		navigation: true,
		pagination: false,
		slideSpeed: 1000,
		stopOnHover: true,
		autoPlay: false,
		items: 1,
		itemsDesktopSmall: [1024, 1],
		itemsTablet: [600, 1],
		itemsMobile: [479, 1]
	});
	var owl = $("#property-slider");
	owl.owlCarousel({
		navigation: true,
		pagination: false,
		slideSpeed: 1000,
		stopOnHover: true,
		autoPlay: true,
		items: 1,
		itemsDesktopSmall: [1024, 1],
		itemsTablet: [600, 1],
		itemsMobile: [479, 1]
	});

	var newProducts = $('#listing-carousel');
	newProducts.find('.owl-prev').html('<i class="lni-chevron-left"></i>');
	newProducts.find('.owl-next').html('<i class="lni-chevron-right"></i>');
	var touchSlider = $('#property-slider');
	touchSlider.find('.owl-prev').html('<i class="lni-chevron-left"></i>');
	touchSlider.find('.owl-next').html('<i class="lni-chevron-right"></i>');
	var testiCarousel = $('.testimonials-carousel');
	testiCarousel.find('.owl-prev').html('<i class="lni-chevron-left"></i>');
	testiCarousel.find('.owl-next').html('<i class="lni-chevron-right"></i>');

		
}
	(jQuery));