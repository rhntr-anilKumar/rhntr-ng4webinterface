/*! 
 * jQuery Steps v1.1.0 - 09/04/2014
 * Copyright (c) 2014 Rafael Staib (http://www.jquery-steps.com)
 * Licensed under MIT http://www.opensource.org/licenses/MIT
 */
(function ($, undefined)	{


	$.fn.extend({
		id: function(	)	{
			return this.attr("id");
		}
	});


	/**
	 * Represents a jQuery wizard plugin.
	 *
	 * @class steps
	 * @constructor
	 * @param [method={}] The name of the method as `String` or an JSON object for initialization
	 * @param [params=]* {Array} Additional arguments for a method call
	 * @chainable
	 **/
	$.fn.steps = function (method)	{

		if ($.fn.steps[method])	{
			return $.fn.steps[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof method === "object" || !method)	{
			return initialize.apply(this, arguments);
		}
		else	{
			$.error("Method " + method + " does not exist on jQuery.steps");
		}
	};

	/**
	 * Initializes the component.
	 *
	 * @static
	 * @private
	 * @method initialize
	 * @param options {Object} The component settings
	 **/
	function initialize(options)	{
		/*jshint -W040 */
		let opts = $.extend(true, {}, defaults, options);

		let wizard = $(this);
		wizard.data('options', opts);

    setElementProperties(wizard);
		
		registerEvent(wizard);
		
		wizard.bind('onStepChange', opts.onStepChange);
	}



	function setElementProperties(wizard)	{

		let opts = wizard.data('options');
		
		getNameSpaceId(wizard);
		
		let bodyTagClassName = opts.bodyClassName;
		let headerTagClassName = opts.headerClassName;
		
		let bodyTag = $('.' + bodyTagClassName);
		let bodyTagChilderns = bodyTag.children();

		let headTag = $('.' + headerTagClassName);
		let headTagChilderns = headTag.children();

		let firstHeadTag = headTagChilderns.first();
		let lastHeadTag = headTagChilderns.last();
				
		let i = 0;
		let bodyTagIds = new Array();
		bodyTagChilderns.each(function(	)	{
			let id = $(this).attr('id');
			if(id === undefined)	{
				id = 'step-body-' + i++;
				$(this).attr('id', id);
			}
			bodyTagIds.push(id);
		});

		i = 0;
		headTagChilderns.each(function(index)	{
			let id = $(this).attr('id');
			if(id === undefined)	{
				id = 'step-header-' + i++;
				$(this).attr('id', id);
			}
			$(this).addClass('step-disabled');
			$(this).children().addClass('step-disabled');
			$(this).attr('step-target', bodyTagIds[index]);
			$(this).attr('step-index', index);
		});
				
		// hide all but not first one.
		bodyTagChilderns.hide();
		bodyTagChilderns.first().show();

		firstHeadTag.addClass('first current');
		lastHeadTag.addClass('last');
		firstHeadTag.removeClass('step-disabled');
		firstHeadTag.children().removeClass('step-disabled');
		
		let nextTag = $('#' + opts.actionLabelIds.next);
		let previousTag = $('#' + opts.actionLabelIds.previous);
		let finishedTag = $('#' + opts.actionLabelIds.finished);
		nextTag.show();
		previousTag.hide();
		finishedTag.hide();
		
		// put all tag reference in wizard
		let tags = new Map();

 		tags.set('bodyTag', bodyTag);
 		tags.set('bodyTagChilderns', bodyTagChilderns);
		
 		tags.set('headTag', headTag);
		tags.set('headTagChilderns', headTagChilderns);
		tags.set('firstHeadTag', firstHeadTag);
		tags.set('lastHeadTag', lastHeadTag);
		
		tags.set('nextTag', nextTag);
		tags.set('previousTag', previousTag);
		tags.set('finishedTag', finishedTag);
		wizard.data('tags', tags);		
	}

	function registerEvent(wizard)	{
	
		let options = wizard.data('options');
		let tags = wizard.data('tags');
	
 		let bodyTag = tags.get('bodyTag');
 		let bodyTagChilderns = tags.get('bodyTagChilderns');
		
 		let headTag = tags.get('headTag');
		let headTagChilderns = tags.get('headTagChilderns');
		let firstHeadTag = tags.get('firstHeadTag');
		let lastletHeadTag = tags.get('lastHeadTag');

		let nextTag = tags.get('nextTag');
		let previousTag = tags.get('previousTag');
		let finishedTag = tags.get('finishedTag');

		nextTag.click(function()	{
			let activeTab = null;
			let activeTabIndex;
			headTagChilderns.each(function(index)	{
				if($(this).hasClass('current'))	{
					activeTab = $(this);
					activeTabIndex = index;
				}
			});
			
			if(!activeTab.hasClass('last'))	{
				let nextActiveTab = activeTab.next();
				activeTab.removeClass('current');
				nextActiveTab.addClass('current');
				nextActiveTab.removeClass('step-disabled');
				nextActiveTab.children().removeClass('step-disabled')
				previousTag.show();
			
				$('#' + activeTab.attr('step-target')).hide();
				$('#' + nextActiveTab.attr('step-target')).show();
				
				if(nextActiveTab.hasClass('last'))	{
					nextTag.hide();
					finishedTag.show();
				}
				options.preTabId = activeTab.attr('id');
				options.curTabId = nextActiveTab.attr('id');
			}			
			wizard.triggerHandler('onStepChange');
		});


		previousTag.click(function()	{		
			let activeTab = null;
			let activeTabIndex;
			headTagChilderns.each(function(index)	{
				if($(this).hasClass('current'))	{
					activeTab = $(this);
					activeTabIndex = index;
				}
			});
			
			if(!activeTab.hasClass('first'))	{
				let nextActiveTab = activeTab.prev();
				activeTab.removeClass('current');
				nextActiveTab.addClass('current');
				finishedTag.hide();
				nextTag.show();

				$('#' + activeTab.attr('step-target')).hide();
				$('#' + nextActiveTab.attr('step-target')).show();
				
				if(nextActiveTab.hasClass('first'))	{
					previousTag.hide();
				}
				
				options.preTabId = activeTab.attr('id');
				options.curTabId = nextActiveTab.attr('id');				
			}
			wizard.triggerHandler('onStepChange');			
		});
		
		headTagChilderns.click(function()	{
			
			let curActiveTab = $('#' + options.curTabId);
							
			if(!$(this).hasClass('step-disabled') 
				&& curActiveTab.attr('id') != $(this).attr('id'))	{
			
				$('#' + $(this).attr('step-target')).show();
				
				$('#' + curActiveTab.attr('step-target')).hide();
				
				$(this).addClass('current');
				curActiveTab.removeClass('current');
				
				options.preTabId = curActiveTab.attr('id');
				options.curTabId = $(this).attr('id');
				
				if($(this).hasClass('first'))	{
					previousTag.hide();
					nextTag.show();
					finishedTag.hide();
				} else if($(this).hasClass('last'))	{
					previousTag.show();
					nextTag.hide();
					finishedTag.show();
				} else {
					previousTag.show();
					nextTag.show();
					finishedTag.hide();
				}
			}
			wizard.triggerHandler('onStepChange');
		});
		
		wizard.bind('onStepChange', options.onStepChange);
	}


	function getNameSpaceId(wizard)	{

		var nameSpaceId = wizard.data("stepNameSpace");
		if (nameSpaceId == null)	{
			nameSpaceId = wizard.id();
			if(nameSpaceId == null)	{
				let options =  wizard.data('options');
				nameSpaceId = "steps-uid-" + options.uniqueDivId;
				wizard.attr('id', nameSpaceId);
				options.uniqueDivId++;
			}
			wizard.data("stepNameSpace", '#' + nameSpaceId);
		}
		return wizard.data('stepNameSpace');
	}

	/**
	 * An object that represents the default settings.
	 * There are two possibities to override the sub-properties.
	 * Either by doing it generally (global) or on initialization.
	 *
	 * @static
	 * @class defaults
	 * @for steps
	 * @example
	 *   // Global approach
	 *   $.steps.defaults.headerTag = "h3";
	 * @example
	 *   // Initialization approach
	 *   $("#wizard").steps({ headerTag: "h3" });
	 **/
	var defaults = $.fn.steps.defaults = {
		
		headerClassName: 'header',
		
		bodyClassName: 'body',

		curTabId: null,
		
		preTabId: null,
		
		uniqueDivId: 0,
		
		actionLabelIds: {
			next: 'next',
			previous: 'previous', 
			finished: 'finished'
		},
		onStepChange: function()	{	}
	};
		
})(jQuery);