
/**
 * Custom js for interactive advanced module.
 * https://inspirationalpixels.com/tutorials/custom-popup-modal
 * 
 */

$(function() {

  /**
   * Popup windows for sign in/up window.
   */
  //----- OPEN EVENT
  $('[data-popup-open]').on('click', function(e)  {
    var targeted_popup_class = $(this).attr('data-popup-open');
    $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
    e.preventDefault();
  });
  
  //----- CLOSE EVENT
  $('[data-popup-close]').on('click', function(e)  {
    var targeted_popup_class = $(this).attr('data-popup-close');
    $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
    e.preventDefault();
  });

  /**
   * Toggle the tabs menu on selection
   * Note : Add toggle-tabs class as first in class names.
   */
  $('.toggle-tabs li a').click(function( ) {
    $(this).addClass('active');
    $(this).parent().siblings().find('a').removeClass('active');
  });
});