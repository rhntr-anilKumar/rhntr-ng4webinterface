
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    document.write('<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgH_mtAbFG45-nRqS2pS1hPtztABtCTJQ&libraries=places&callback=initAutocomplete" async defer></script>');

    var placeSearch, autocomplete, autocomplete2;
    var componentForm = {
        street_number: 'long_name',
        route: 'long_name',
        sublocality_level_2: 'long_name',
        sublocality_level_1: 'long_name',
        locality: 'long_name',
        administrative_area_level_2: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name',
        postal_code: 'long_name'
    };

    
    var latestRef = null;

    function initAutocomplete(addressHolderId) {

      if(addressHolderId == null || addressHolderId == undefined) {
        addressHolderId = latestRef;// return;
      }
      latestRef = addressHolderId;

      // Create the autocomplete object, restricting the search to geographical
      // location types.
      autocomplete = new google.maps.places.Autocomplete(/** @type {!HTMLInputElement} */ 
          (document.getElementById(addressHolderId)), {types: ['geocode']});

      // When the user selects an address from the dropdown, populate the address
      // fields in the form.
      autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
      // Get the place details from the autocomplete object.
      var place = autocomplete.getPlace();

      for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
      }

      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              document.getElementById(addressType).value = val;
          }
      }

      document.getElementById('latitude').value = place.geometry.location.lat();
      document.getElementById('longitude').value = place.geometry.location.lng();
      document.getElementById('placeId').value = place.place_id;
    }
  
  // Bias the autocomplete object to the user's geographical location,
  // as supplied by the browser's 'navigator.geolocation' object.
  function geolocate() {
      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
          var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
          };
          var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
          });
          autocomplete.setBounds(circle.getBounds());
          autocomplete2.setBounds(circle.getBounds());
          });
      }
  }  

  /**
   * Loading Google Map location suggestion drop down options
   * directly from *.ts file of angular 4.
   * So exposing these methos to angulat application controller.
   */

  (function ($, undefined)	{

    /**
     * For location suggestion drop down option.
     */
    $.fn.initAutocomplete = function (addressHolderId)	{
      initAutocomplete(addressHolderId);
    }
  })(jQuery);