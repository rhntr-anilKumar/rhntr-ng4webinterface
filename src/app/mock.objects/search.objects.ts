import { SearchInputs } from "../models/entities/search.inputs.module";


export class SearchObjects {

  public static searchInputs(propertyType)  {
    let searchInputs = new SearchInputs(); 
    searchInputs.latitude = '';
    searchInputs.longitude = '';
    searchInputs.placeId = '';
    searchInputs.formattedAddress = "Electronic City Phase 1, Bengaluru, Karnataka 560003, India";
    searchInputs.route = '';
    searchInputs.sublocalityLevel2 = '';
    searchInputs.sublocalityLevel1 = "Electronic City";
    searchInputs.locality = "Bengaluru";
    searchInputs.administrativeAreaLevel2 = "Bangalore Urban";
    searchInputs.administrativeAreaLevel1 = "Karnataka";
    searchInputs.country = "India";
    searchInputs.postalCode = "560100";
    searchInputs.propertyType = propertyType;
    return searchInputs;
  }  
}
