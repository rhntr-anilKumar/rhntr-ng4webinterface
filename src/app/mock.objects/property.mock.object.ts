
export class PropertyObjects  {

  public static homeObjects = {
    "id": 1527446843599,
    "objectMap": {
      "addressList": [2, 3],
      "inputs": {
        "id": 1527446844131,
        "longitude": 7.57075,
        "latitude": 12.990489,
        "placeId": "ChIJy2aM9iIWrjsRPRgNzAqF9mo",
        "formattedAddress": "Platform Rd, Jai Bheema Nagar, Sheshadripuram, Bengaluru, Karnataka 560003, India",
        "streetNumber": null,
        "street": null,
        "route": "Platform Road",
        "sublocalityLevel2": "Jai Bheema Nagar",
        "sublocalityLevel1": "Sheshadripuram",
        "locality": "Bengaluru",
        "administrativeAreaLevel2": "Bangalore Urban",
        "administrativeAreaLevel1": "Karnataka",
        "country": "India",
        "postalCode": "560003",
        "propertyType": "HOME",
        "searchInput": null,
        "inputSource": null,
        "objectMap": {}
      },
      "properties": [
          {
            "id": 1,
            "nonVegAllowed": true,
            "onlyFamilyOriented": true,
            "availableRooms": 0,
            "totalRooms": 1,
            "petsAllowed": false,
            "parkingSpace": true,
            "spaceProperties": [
              {
                "id" : 1,
                "floor" : 2,
                "furnished": "SEMI",
                "bhkLength": 3,
                "deposit": 50000,
                "monthlyRent": 10000,
                "availability": 1,
                "negotiable": true
              }                
            ],
            "furnished": true,
            "constructedOn": "27-01-1991",
            "addressSpace": {
                "id": 1,
                "latitude": 12.848162,
                "longitude": 77.67072,
                "placeId": "ChIJHXRhipFsrjsRgBrZtVaE4Ys",
                "formattedAddress": "Konappana Agrahara, Electronic City, Bengaluru, Karnataka 560100, India",
                "streetNumber": null,
                "street": null,
                "route": null,
                "sublocalityLevel2": "Konappana Agrahara",
                "sublocalityLevel1": "Electronic City",
                "locality": "Bengaluru",
                "administrativeAreaLevel2": "Bangalore Urban",
                "administrativeAreaLevel1": "Karnataka",
                "country": "India",
                "postalCode": "560100",
                "propertyType": "HOME",
                "userId": 1
            }
        },
        {
          "id": 2,
          "nonVegAllowed": true,
          "onlyFamilyOriented": false,
          "availableRooms": 2,
          "totalRooms": 2,
          "petsAllowed": false,
          "parkingSpace": false,
          "spaceProperties": [
            {
              "id" : 1,
              "floor" : 3,
              "furnished": "SEMI",
              "bhkLength": 1,
              "deposit": 50000,
              "monthlyRent": 10000,
              "availability": 1,
              "negotiable": true
            }            
          ],
          "furnished": true,
          "constructedOn": "10-10-1995",
          "addressSpace": {
            "id": 2,
            "latitude": 12.990489,
            "longitude": 77.57075,
            "placeId": "ChIJy2aM9iIWrjsRPRgNzAqF9mo",
            "formattedAddress": "Platform Rd, Jai Bheema Nagar, Sheshadripuram, Bengaluru, Karnataka 560003, India",
            "streetNumber": null,
            "street": null,
            "route": "Platform Road",
            "sublocalityLevel2": "Jai Bheema Nagar",
            "sublocalityLevel1": "Sheshadripuram",
            "locality": "Bengaluru",
            "administrativeAreaLevel2": "Bangalore Urban",
            "administrativeAreaLevel1": "Karnataka",
            "country": "India",
            "postalCode": "560003",
            "propertyType": "HOME",
            "userId": 2
          }
        },
        {
          "id": 3,
          "nonVegAllowed": false,
          "onlyFamilyOriented": true,
          "availableRooms": 0,
          "totalRooms": 1,
          "petsAllowed": false,
          "parkingSpace": true,
          "spaceProperties": [
            {
              "id" : 1,
              "floor" : 2,
              "furnished": "SEMI",
              "bhkLength": 2,
              "deposit": 50000,
              "monthlyRent": 10000,
              "availability": 1,
              "negotiable": true
            }
          ],
          "furnished": false,
          "constructedOn": "01-02-2001",
          "addressSpace": {
            "id": 3,
            "latitude": 12.990489,
            "longitude": 77.57075,
            "placeId": "ChIJy2aM9iIWrjsRPRgNzAqF9mo",
            "formattedAddress": "Platform Rd, Jai Bheema Nagar, Sheshadripuram, Bengaluru, Karnataka 560003, India",
            "streetNumber": null,
            "street": null,
            "route": "Platform Road",
            "sublocalityLevel2": "Jai Bheema Nagar",
            "sublocalityLevel1": "Sheshadripuram",
            "locality": "Bengaluru",
            "administrativeAreaLevel2": "Bangalore Urban",
            "administrativeAreaLevel1": "Karnataka",
            "country": "India",
            "postalCode": "560003",
            "propertyType": "HOME",
            "userId": 1
          }
        }
      ]
    }
  };
}