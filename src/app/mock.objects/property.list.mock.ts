import { HomeListing } from "../models/entities/home.prop.list";

export class PropertiesListMock {

  public static getPopulatHomesList() :HomeListing[] {
    let popularList = new Array();

    let list = new HomeListing();
    list.id = 1;
    list.availability = 1;
    list.bhkLength = '3BKH';
    list.negotiable = true;
    list.monthlyRent = 10000;
    list.deposit = 100000;
    list.formattedAddress = 'Electronic City Bangalore 560 100';
    popularList.push(list);
    
    list = new HomeListing();
    list.id = 2;
    list.availability = 2;
    list.bhkLength = '2BKH';
    list.negotiable = true;
    list.monthlyRent = 8000;
    list.deposit = 50000;
    list.formattedAddress = 'Electronic City Bangalore 560 100';
    popularList.push(list);    
    
    list = new HomeListing();
    list.id = 3;
    list.availability = 2;
    list.bhkLength = '1BKH';
    list.negotiable = true;
    list.monthlyRent = 5000;
    list.deposit = 50000;
    list.formattedAddress = 'Shanti Nagar Bangalore 560 100';
    popularList.push(list);
    
    list = new HomeListing();
    list.id = 4;
    list.availability = 4;
    list.bhkLength = '2BKH';
    list.negotiable = true;
    list.monthlyRent = 9000;
    list.deposit = 50000;
    list.formattedAddress = 'Chamaraj pet Bangalore 560 100';
    popularList.push(list);
    
    list = new HomeListing();
    list.id = 5;
    list.availability = 4;
    list.bhkLength = '1BKH';
    list.negotiable = true;
    list.monthlyRent = 6000;
    list.deposit = 50000;
    list.formattedAddress = 'Rajaji Nagar Bangalore 560 100';
    popularList.push(list);
    
    list = new HomeListing();
    list.id = 6;
    list.availability = 4;
    list.bhkLength = '2BKH';
    list.negotiable = true;
    list.monthlyRent = 12000;
    list.deposit = 80000;
    list.formattedAddress = 'Vijay Nagar Bangalore 560 100';
    popularList.push(list);
    
    return popularList;
  }  
}
