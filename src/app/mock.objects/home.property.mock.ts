
export class HomePropertyMock  {

  public static propertyList: any =  {
    "id": 1537000995773,
    "objectMap": {
        "effectiveGoogledAddressIds": [
            68,
            1,
            40,
            41,
            49
        ],
        "inputs": {
            "id": 1537000995773,
            "longitude": null,
            "latitude": null,
            "placeId": null,
            "formattedAddress": null,
            "streetNumber": null,
            "street": null,
            "route": null,
            "sublocalityLevel2": null,
            "sublocalityLevel1": "Electronic City",
            "locality": "Bengaluru",
            "administrativeAreaLevel2": "Bangalore Urban",
            "administrativeAreaLevel1": "Karnataka",
            "country": "India",
            "postalCode": null,
            "propertyType": "PG",
            "searchInput": null,
            "inputSource": null,
            "objectMap": {}
        },
        "properties": [
            {
                "id": 53,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 13,
                "totalRooms": 17,
                "advance": 4000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 261,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 262,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 263,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 264,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 265,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 53"
            },
            {
                "id": 75,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 6,
                "totalRooms": 57,
                "advance": 6000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 369,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 370,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 371,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 372,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 373,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 75"
            },
            {
                "id": 77,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 31,
                "totalRooms": 35,
                "advance": 4000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 379,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 380,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 381,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 382,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 383,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 77"
            },
            {
                "id": 91,
                "type": "BOYS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 17,
                "totalRooms": 21,
                "advance": 6000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 449,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 450,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 451,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 452,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 453,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 91"
            },
            {
                "id": 116,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 9,
                "totalRooms": 28,
                "advance": 4000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 574,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 575,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 576,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 577,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 578,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 116"
            },
            {
                "id": 143,
                "type": "GIRLS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 27,
                "totalRooms": 31,
                "advance": 3000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 707,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 708,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 709,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 710,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 711,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 143"
            },
            {
                "id": 160,
                "type": "GIRLS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 17,
                "totalRooms": 47,
                "advance": 5000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 792,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 793,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 794,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 795,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 796,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 160"
            },
            {
                "id": 170,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 1,
                "totalRooms": 52,
                "advance": 4000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 842,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 843,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 844,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 845,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 846,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 170"
            },
            {
                "id": 220,
                "type": "GIRLS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 1,
                "totalRooms": 32,
                "advance": 4500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 1090,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1091,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1092,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1093,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1094,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 220"
            },
            {
                "id": 221,
                "type": "BOYS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 20,
                "totalRooms": 29,
                "advance": 3500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 1095,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1096,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1097,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1098,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1099,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 221"
            },
            {
                "id": 234,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 7,
                "totalRooms": 26,
                "advance": 4500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 1160,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1161,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1162,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1163,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1164,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 234"
            },
            {
                "id": 246,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 17,
                "totalRooms": 45,
                "advance": 6000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 1218,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1219,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1220,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1221,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1222,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 2112,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2113,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2114,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2115,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2116,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 246"
            },
            {
                "id": 252,
                "type": "BOYS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 5,
                "totalRooms": 14,
                "advance": 3000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 1248,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1249,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1250,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1251,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1252,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 2142,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2143,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2144,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2145,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2146,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 252"
            },
            {
                "id": 268,
                "type": "GIRLS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 8,
                "totalRooms": 15,
                "advance": 3500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 1328,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1329,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1330,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1331,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1332,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 2222,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2223,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2224,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2225,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2226,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 268"
            },
            {
                "id": 285,
                "type": "BOYS",
                "foodType": "WITH_FOOD",
                "availableRooms": 0,
                "totalRooms": 60,
                "advance": 4000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 1413,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1414,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1415,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1416,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1417,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 2307,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2308,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2309,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2310,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2311,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 2400,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2401,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2402,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2403,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2404,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 285"
            },
            {
                "id": 291,
                "type": "BOYS",
                "foodType": "WITH_FOOD",
                "availableRooms": 35,
                "totalRooms": 58,
                "advance": 3000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 1443,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1444,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1445,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1446,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1447,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 2337,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2338,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2339,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2340,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2341,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 2430,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2431,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2432,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2433,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2434,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 291"
            },
            {
                "id": 293,
                "type": "GIRLS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 14,
                "totalRooms": 32,
                "advance": 6000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 1453,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1454,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1455,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1456,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1457,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 2347,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2348,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2349,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2350,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2351,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 2440,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2441,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2442,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2443,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2444,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 293"
            },
            {
                "id": 303,
                "type": "BOYS",
                "foodType": "WITH_FOOD",
                "availableRooms": 23,
                "totalRooms": 47,
                "advance": 5500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 1501,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1502,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1503,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1504,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1505,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 2490,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2491,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2492,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2493,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2494,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 303"
            },
            {
                "id": 308,
                "type": "BOYS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 26,
                "totalRooms": 54,
                "advance": 6000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 1526,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1527,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1528,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1529,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1530,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 2515,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2516,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2517,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2518,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2519,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 308"
            },
            {
                "id": 333,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 24,
                "totalRooms": 41,
                "advance": 5500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 2640,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2641,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2642,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2643,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2644,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 1651,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1652,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1653,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1654,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1655,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 333"
            },
            {
                "id": 342,
                "type": "BOYS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 29,
                "totalRooms": 49,
                "advance": 6000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 2683,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2684,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2685,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2686,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2687,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 1696,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1697,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1698,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1699,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1700,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 342"
            },
            {
                "id": 345,
                "type": "GIRLS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 6,
                "totalRooms": 56,
                "advance": 5000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 2698,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2699,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2700,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2701,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2702,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 1711,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1712,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1713,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1714,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1715,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 345"
            },
            {
                "id": 346,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 28,
                "totalRooms": 46,
                "advance": 6000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 2703,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2704,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2705,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2706,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2707,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 1716,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1717,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1718,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1719,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1720,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 346"
            },
            {
                "id": 401,
                "type": "BOYS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 21,
                "totalRooms": 49,
                "advance": 6000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 2978,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2979,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2980,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1989,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 1990,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 1991,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 1992,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 1993,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 401"
            },
            {
                "id": 405,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 6,
                "totalRooms": 17,
                "advance": 4500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 2996,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2997,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2998,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2999,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 3000,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    },
                    {
                        "id": 2009,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 2010,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 2011,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 2012,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 2013,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 405"
            },
            {
                "id": 427,
                "type": "BOYS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 2,
                "totalRooms": 11,
                "advance": 3000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 3106,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 3107,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 3108,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 3109,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 3110,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 427"
            },
            {
                "id": 453,
                "type": "BOYS",
                "foodType": "WITH_FOOD",
                "availableRooms": 1,
                "totalRooms": 20,
                "advance": 5500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 3236,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 3237,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 3238,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 3239,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 3240,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 453"
            },
            {
                "id": 484,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 11,
                "totalRooms": 15,
                "advance": 6000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 3389,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 3390,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 3391,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 3392,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 3393,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 484"
            },
            {
                "id": 503,
                "type": "BOYS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 44,
                "totalRooms": 58,
                "advance": 5000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 3484,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 3485,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 3486,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 3487,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 3488,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 503"
            },
            {
                "id": 530,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 55,
                "totalRooms": 60,
                "advance": 5000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 3617,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 3618,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 3619,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 3620,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 3621,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 530"
            },
            {
                "id": 552,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 7,
                "totalRooms": 18,
                "advance": 3000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 3727,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 3728,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 3729,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 3730,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 3731,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 552"
            },
            {
                "id": 553,
                "type": "BOYS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 1,
                "totalRooms": 21,
                "advance": 4000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 3732,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 3733,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 3734,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 3735,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 3736,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 553"
            },
            {
                "id": 561,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 1,
                "totalRooms": 31,
                "advance": 3000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 3772,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 3773,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 3774,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 3775,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 3776,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 561"
            },
            {
                "id": 575,
                "type": "BOYS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 3,
                "totalRooms": 18,
                "advance": 3500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 3842,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 3843,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 3844,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 3845,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 3846,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 575"
            },
            {
                "id": 594,
                "type": "BOYS",
                "foodType": "WITH_FOOD",
                "availableRooms": 35,
                "totalRooms": 45,
                "advance": 4500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 3935,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 3936,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 3937,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 3938,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 3939,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 594"
            },
            {
                "id": 598,
                "type": "BOYS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 1,
                "totalRooms": 30,
                "advance": 3000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 3955,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 3956,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 3957,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 3958,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 3959,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 598"
            },
            {
                "id": 605,
                "type": "BOYS",
                "foodType": "WITH_FOOD",
                "availableRooms": 10,
                "totalRooms": 31,
                "advance": 4500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 3990,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 3991,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 3992,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 3993,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 3994,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 605"
            },
            {
                "id": 626,
                "type": "BOYS",
                "foodType": "WITH_FOOD",
                "availableRooms": 33,
                "totalRooms": 36,
                "advance": 5000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 4095,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 4096,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 4097,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 4098,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 4099,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 626"
            },
            {
                "id": 653,
                "type": "GIRLS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 2,
                "totalRooms": 27,
                "advance": 3500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 4228,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 4229,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 4230,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 4231,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 4232,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 653"
            },
            {
                "id": 696,
                "type": "GIRLS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 26,
                "totalRooms": 30,
                "advance": 5500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 4443,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 4444,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 4445,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 4446,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 4447,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 696"
            },
            {
                "id": 716,
                "type": "BOYS",
                "foodType": "WITH_FOOD",
                "availableRooms": 0,
                "totalRooms": 27,
                "advance": 3000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 4541,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 4542,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 4543,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 4544,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 4545,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 716"
            },
            {
                "id": 743,
                "type": "BOYS",
                "foodType": "WITH_FOOD",
                "availableRooms": 4,
                "totalRooms": 28,
                "advance": 4000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 4676,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 4677,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 4678,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 4679,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 4680,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 743"
            },
            {
                "id": 748,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 29,
                "totalRooms": 59,
                "advance": 4500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 4701,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 4702,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 4703,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 4704,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 4705,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 748"
            },
            {
                "id": 817,
                "type": "BOYS",
                "foodType": "WITH_FOOD",
                "availableRooms": 7,
                "totalRooms": 60,
                "advance": 6000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 5044,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 5045,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 5046,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 5047,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 5048,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 817"
            },
            {
                "id": 831,
                "type": "GIRLS",
                "foodType": "WITH_FOOD",
                "availableRooms": 15,
                "totalRooms": 51,
                "advance": 4000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 5112,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 5113,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 5114,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 5115,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 5116,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 831"
            },
            {
                "id": 838,
                "type": "BOYS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 11,
                "totalRooms": 37,
                "advance": 4000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 5147,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 5148,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 5149,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 5150,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 5151,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 838"
            },
            {
                "id": 842,
                "type": "BOYS",
                "foodType": "WITH_FOOD",
                "availableRooms": 6,
                "totalRooms": 24,
                "advance": 3000,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 5167,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 5168,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 6500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 5169,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 5170,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 5171,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 3500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 842"
            },
            {
                "id": 846,
                "type": "BOYS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 4,
                "totalRooms": 11,
                "advance": 4500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 5187,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 7000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 5188,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 5189,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 5190,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 5000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 5191,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 846"
            },
            {
                "id": 867,
                "type": "GIRLS",
                "foodType": "WITH_OUT_FOOD",
                "availableRooms": 6,
                "totalRooms": 15,
                "advance": 4500,
                "underCCTVServilence": false,
                "securityGuardianAreExist": false,
                "spaceProperties": [
                    {
                        "id": 5292,
                        "sharingType": "SINGLE_SHARING",
                        "monthlyRent": 6000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 1
                    },
                    {
                        "id": 5293,
                        "sharingType": "DOUBLE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 2
                    },
                    {
                        "id": 5294,
                        "sharingType": "THREE_SHARING",
                        "monthlyRent": 5500,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 3
                    },
                    {
                        "id": 5295,
                        "sharingType": "FOUR_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 4
                    },
                    {
                        "id": 5296,
                        "sharingType": "OPEN_SHARING",
                        "monthlyRent": 4000,
                        "totalRooms": null,
                        "availableRooms": null,
                        "sharing": 5
                    }
                ],
                "pgName": "PG 867"
            }
        ]
    }
  };
}