
class HomeSpaceProperties   {
  id: number;
	floor: number;
	furnished: String;
	bhkLength: String;
	deposit: number;
	rent: number;
	availability: number;
	negotiable: boolean;
}