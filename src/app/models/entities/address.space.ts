
export class AddressSpace   {

  latitude: String;
  longitude: String;
  placeId: String;
  formattedAddress: String;

  streetNumber: String;
  street: String;
  route: String;
  sublocalityLevel2: String;
  sublocalityLevel1: String;
  locality: String;
  administrativeAreaLevel2: String;
  administrativeAreaLevel1: String;
  country: String;
  postalCode: String;
  
  propertyType: String;
}