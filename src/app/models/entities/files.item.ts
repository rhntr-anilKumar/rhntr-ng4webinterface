
export class FilesItem  {
  public fileName: String;
  public originalFileName: String;
  public fileDownloadUri: String;
  public fileType: String;
  public size: Number;
}