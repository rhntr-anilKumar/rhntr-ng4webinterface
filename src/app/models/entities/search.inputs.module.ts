
export class SearchInputs   {

  streetNumber: String;
  street: String;
  route: String;
  sublocalityLevel2: String;
  sublocalityLevel1: String;
  locality: String;
  administrativeAreaLevel2: String;
  administrativeAreaLevel1: String;
  country: String;
  postalCode: String;
  
  latitude: String;
  longitude: String;
  placeId: String;
  formattedAddress: String;

  propertyType: String;
}