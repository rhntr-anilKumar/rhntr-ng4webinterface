
export class PGSpaceProperties  {  
  id: Number;
  sharingType: String;
  monthlyRent: Number;
  totalRooms: Number;
  availableSpace: Number;
  availabile: boolean;
  sharing: Number;
}