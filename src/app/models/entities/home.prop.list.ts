

export class HomeListing   {
  id: number;
  formattedAddress: string;
  
  bhkLength: String;
  deposit: number;
  monthlyRent: number;
  availability: number;
  negotiable: boolean;
}