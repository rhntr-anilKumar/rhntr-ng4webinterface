import { AddressSpace } from './address.space';
import { User } from './user.model';

export class Apartment   {

  id: Number;
  name: String;
  constructedOn: Date;
  street: String;
  landMark: String;
  address: String;

  nonVegAllowed: Boolean;
  petsAllowed: Boolean;
  parking: Boolean;

  tenent: String;
  buildUpArea: Number;
  bhkLength: Number;
  furnished: String;
  floor: String;
  facing: String;
  amenities: String[];

  rent: Number;
  deposit: Number;
  negotiable: Boolean;
  depositRefundable: Boolean;
  maintainanceCharge: Number;
  agreemantDuriation: Number
  leavingIntimation: String;

  addressSpace: AddressSpace;
  user: User;
}


export var ApartmentOptionsList = {
  AMENTIIES_LIST: [
    {text: 'Common TV',       value: 'TV',            checked: false, imgClass: 'tv-ico'},
    {text: 'AC',              value: 'AC',            checked: false, imgClass: 'ac-ico'},
    {text: 'Cupboard',        value: 'CUPBOARD',      checked: false, imgClass: 'cupboard-ico'},
    {text: 'Attached Bath',   value: 'ATTCBATH',      checked: false, imgClass: 'bath-ico'},
    {text: 'WIFI',            value: 'WIFI',          checked: false, imgClass: 'wifi-ico'},
    {text: 'Refrigerator',    value: 'REFRIGERATOE',  checked: false, imgClass: 'refrig-ico'},
    {text: 'Lift',            value: 'LIFT',          checked: false, imgClass: 'lift-ico'},
    {text: 'Gyser',           value: 'GYSER',         checked: false, imgClass: 'gyser-ico'},
    {text: 'Cooking Allowed', value: 'COOKING',       checked: false, imgClass: 'cooking-ico'},
    {text: 'Power Backup',    value: 'PWRBCKUP',      checked: false, imgClass: 'powerback-ico'}    
  ]
}