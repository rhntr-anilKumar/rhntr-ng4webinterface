import { AddressSpace } from "./address.space";
import { FilesItem } from "./files.item";
import { User } from "./user.model";

export class Home {

  id: Number;
  name: String;
  constructedOn: Date;
  street: String;
  landMark: String;

  nonVegAllowed: Boolean;
  petsAllowed: Boolean;
  parking: Boolean;

  tenant: String;
  buildUpArea: Number;
  bhkLength: Number;
  furnished: String;
  floor: String;
  facing: String;
  amenities: String[];

  rent: Number;
  deposit: Number;
  negotiable: Boolean;
  depositRefundable: Boolean;
  maintainanceCharge: Number;
  agreemantDuriation: Number;
  leavingIntimation: String;

  spaceProperties: HomeSpaceProperties;
  user: User;

  homePictures: FilesItem[];

  addressSpace: AddressSpace;
  formattedAddress: String;
}


export var HomePropertyTypeOptionMap = new Map();

HomePropertyTypeOptionMap.set('SINGLE_ROOM', 'Single Room');
HomePropertyTypeOptionMap.set('RK', 'RK');
HomePropertyTypeOptionMap.set('ONE_BHK', '1 BHK');
HomePropertyTypeOptionMap.set('TWO_BHK', '2 BHK');
HomePropertyTypeOptionMap.set('THREE_BHK', '3 BHK');
HomePropertyTypeOptionMap.set('FOUR_BHK', '4 BHK');
HomePropertyTypeOptionMap.set('ANY_BHK', '5+ BHK');
HomePropertyTypeOptionMap.set('VILLA', 'Villa');


export var FurnishedTypeOptionMap = new Map();

FurnishedTypeOptionMap.set('UNFURNISHED', 'Unfurished');
FurnishedTypeOptionMap.set('SEMI_FURNISHED', 'Semi Furnished');
FurnishedTypeOptionMap.set('FULL_FURNISHED', 'Full Furnished');

export var HomeOptionsList = {

  bhkLength: [
    {text: '1 RK',    value: 'RK',        checked: false},
    {text: '1 BHK',   value: 'ONE_BHK',   checked: false},
    {text: '2 BHK',   value: 'TWO_BHK',   checked: false},
    {text: '3 BHK',   value: 'THREE_BHK', checked: false},
    {text: '4 BHK',   value: 'FOUR_BHK',  checked: false},
    {text: '5+ BHK',  value: 'ANY_BHK',   checked: false}
  ],

  tenant: [
    {text: 'Bachelors', value: 'BACHELORS', checked: false},
    {text: 'Family',    value: 'FAMILY',    checked: false},
    {text: 'Any',       value: 'ANY',       checked: false},
  ],

  furnished: [
    {text: 'None',  value: 'UNFURNISHED',     checked: false},
    {text: 'Semi',  value: 'SEMI_FURNISHED',  checked: false},
    {text: 'Full',  value: 'FULL_FURNISHED',  checked: false}
  ],

  floors: [
    {text: 'Ground',  value: '0',  checked: false},
    {text: 'First',   value: '1',  checked: false},
    {text: 'Second',  value: '2',  checked: false},
    {text: 'Third',   value: '3',  checked: false},
    {text: 'Fourth',  value: '4',  checked: false},
    {text: 'Fifth',   value: '5',  checked: false},
    {text: 'Any',     value: '5+',  checked: false},
  ],

  amenities : [
    {text: 'Gym',           value: 'LIFT',          checked: false, imgClass: 'lift-ico'},
    {text: 'Lift',          value: 'LIFT',          checked: false, imgClass: 'lift-ico'},
    {text: 'Gyser',         value: 'GYSER',         checked: false, imgClass: 'gyser-ico'},
    {text: 'Power Backup',  value: 'PWRBCKUP',      checked: false, imgClass: 'powerback-ico'}
  ],

  parking: [
    {text: 'Bike',  value: 'BIKE',  checked: false},
    {text: 'Car',   value: 'CAR',   checked: false},    
  ]
};

