import { AddressSpace } from "./address.space";
import { FilesItem } from "./files.item";
import { PGSpaceProperties } from "./pg.space.properties";
import { User } from "./user.model";


export class PayingGuest   {
  
  id: number;

  type: String;
  name: String;

  foodType: String;
  availableRooms: number;
  totalRooms: number;
  advance: number;
  sharings: Number[];
  parking: Boolean;
  refundable: Boolean;
  negotiable: Boolean;
  underCCTVServilence: boolean;
  securityGuardianAreExist: boolean;
  spaceProperties: PGSpaceProperties[];
  user: User;
  addressSpace: AddressSpace;
  pgPictures: FilesItem[];
  curPicURL: String;

  amenities: String[];
  services: String[];

  sharingType: String;
  monthlyRent: Number;
  sharing: Number;

}
