import { AddressSpace } from "./address.space";

export class User   {
  id: Number;
	username: String;
  passcode: String;
  confirmPasscode: String;
	firstName: String;
	lastName: String;
	dateOfBirth: String;
	emailId: String;	
  phoneNumber: String;
  addressSpaces: AddressSpace[]
}