

export class PgFilter  {  
  pgType: string[];
  minRate: Number = 2000;
  maxRate: Number = 20000;
  sharings: Number[];
  withFood: boolean;
  withoutFood: boolean;
  cctv: boolean;
  securityGaurd: boolean;
  parking: boolean;
}



export var PgOptionsList = {
  pgTypes : [
    {text:'Girls',  value:'GIRLS',  checked: false},
    {text:'Boys',   value:'BOYS',   checked: false}
  ],

  sharings :  [
    {text:'1',  value:'1',  checked: false},
    {text:'2',  value:'2',  checked: false},
    {text:'3',  value:'3',  checked: false},
    {text:'4',  value:'4',  checked: false},
    {text:'5',  value:'5',  checked: false},
    {text:'5+', value:null, checked: false}
  ],

  sharingOptionsForPostProperty : [
    {text: 'One',  value: 'SINGLE_SHARING',   checked: false,  imgClass: 'signle_room'},
    {text: 'Two',  value: 'DOUBLE_SHARING',   checked: false,  imgClass: 'double_room'},
    {text: 'Three',   value: 'THREE_SHARING',   checked: false,  imgClass: 'three_room'},
    {text: 'Four',    value: 'FOUR_SHARING',   checked: false,  imgClass: 'four_room'},
    {text: 'Five',    value: 'FIVE_SHARING',   checked: false,  imgClass: 'five_room'},
    {text: 'Open',    value: 'OPEN_SHARING',  checked: false,  imgClass: 'six_room'}
  ],

  availbleServices : [
    {text: 'Laundary',        value: 'LAUNDARY',        checked: false},
    {text: 'Room Cleaning',   value: 'ROOMCLEANING',    checked: false},
    {text: 'Warden Facility', value: 'WARDENFACILITY',  checked: false}
  ],

  amenitiesList : [
    {text: 'Common TV',       value: 'TV',            checked: false, imgClass: 'tv-ico'},
    {text: 'AC',              value: 'AC',            checked: false, imgClass: 'ac-ico'},
    {text: 'Cupboard',        value: 'CUPBOARD',      checked: false, imgClass: 'cupboard-ico'},
    {text: 'Attached Bath',   value: 'ATTCBATH',      checked: false, imgClass: 'bath-ico'},
    {text: 'WIFI',            value: 'WIFI',          checked: false, imgClass: 'wifi-ico'},
    {text: 'Refrigerator',    value: 'REFRIGERATOE',  checked: false, imgClass: 'refrig-ico'},
    {text: 'Lift',            value: 'LIFT',          checked: false, imgClass: 'lift-ico'},
    {text: 'Gyser',           value: 'GYSER',         checked: false, imgClass: 'gyser-ico'},
    {text: 'Cooking Allowed', value: 'COOKING',       checked: false, imgClass: 'cooking-ico'},
    {text: 'Power Backup',    value: 'PWRBCKUP',       checked: false, imgClass: 'powerback-ico'}
  ]
};

