

export class HomeFilter  {
  bhkLength: String[];
  tenant: String[];
  minRate: Number = 2000;
  maxRate: Number = 20000;
  furnished: String[];
  amenities: String[];
  floor: String[];
  parking: boolean;
  securityGuard: boolean;
}