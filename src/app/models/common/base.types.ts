
export var PropertyType =  {
  APARTMENT : 'Apartment',
  HOME : 'HOME',
  PG : 'PG',
  OFFICE_SPACE : 'OFFICESPACE',
  SHOPS : 'SHOPS',
  WAREHOUSE : 'WAREHOUSE',
  PARTY_HALL: 'PARTHALL'
}
