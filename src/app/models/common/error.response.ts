

export class ErrorResponse {
  public headers: any;
	public status: Number;
	public statusText: String;
  public url: String;
  public ok: Boolean;
	public name: String;
  public message: String;
  public error: Error;
}


export class Error {
  public timestamp: Number;
  public status: Number;
  public statusText: String;
	public messages: String[];
	public errors: Errors[];
}


export class Errors  {
  public objcet: String;
  public field: String;
  public message: String;
}