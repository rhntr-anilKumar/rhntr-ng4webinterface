/** Angular package list  */
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

/** Routing map list  */
import { AppRoutingMap } from './app.routers.config/app.router.table';

/** Components list */
import { AppComponent } from './app.component';
import { AboutUsComponent } from './components/static/about-us/about-us.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { HeaderComponent } from './components/header/header.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { HomePropertyViewComponent } from './components/property-viewer/home-property-view/home-property-view.component';
import { PhotoBoxComponent } from './components/post-property/photo-box/photo-box.component';
import { PhotoViewerComponent } from './components/photo-viewer/photo-viewer.component';
import { PostPropertyComponent } from './components/post-property/post-property.component';
import { ProfileComponent } from './components/profile/profile.component';
import { PropertyApartmentComponent } from './components/post-property/property-apartment/property-apartment.component';
import { PropertyHomeComponent } from './components/post-property/property-home/property-home.component';
import { PropertyPgComponent } from './components/post-property/property-pg/property-pg.component';
import { SearchFiltersComponent } from './components/search-page/search-filters/search-filters.component';
import { SearchEngineComponent } from './components/search-engine/search-engine.component';
import { SearchPageComponent } from './components/search-page/search-page.component';
import { TestComponent } from './components/test/test.component';
import { UploadFileService } from './services/upload.file.service';

/** Services list */
import { AuthorizationService } from './services/authorization.service';
import { PostPropertyService } from './services/post.property.service';
import { PropertyService } from './services/property.service';
import { SearchService } from './services/search.service';

/** Pipe/Filters list */
import { HomeFilterPipe } from './pipes/home.filter.pipe';
import { PgFilterPipe } from './pipes/pg.filter.pipe';

import { environment } from '../environments/environment';

//import { ProfileComponent } from './profile/profile.component';

@NgModule({
  declarations: [
    AboutUsComponent,
    AppComponent,
    HeaderComponent,
    HomeFilterPipe,
    HomePageComponent,
    HomePropertyViewComponent,
    LogInComponent,
    PgFilterPipe,
    PhotoBoxComponent,
    PhotoViewerComponent,
    PostPropertyComponent,
    ProfileComponent,
    PropertyApartmentComponent,
    PropertyHomeComponent,
    PropertyPgComponent,
    SearchEngineComponent,
    SearchFiltersComponent,
    SearchPageComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(AppRoutingMap)
  ],
  providers: [
    AuthorizationService,
    PostPropertyService,
    PropertyService,
    SearchService,
    UploadFileService
  ],
  bootstrap: [AppComponent]
})

/**
 * Rent Hunter Application....
 */
export class AppModule {

  constructor() {
    console.log('Application Module Started. listening @ ' + environment.REST_API_BASE_PATH);
    console.log('Application Module : Rent Hunter Application for rent hunting remotly...');
  }
}