import { Component, OnInit } from '@angular/core';

import { AuthorizationService } from '../../services/authorization.service';
import { Home, HomePropertyTypeOptionMap } from '../../models/entities/home';
import { HomeListing } from '../../models/entities/home.prop.list';
import { jScript } from '../../common/jscript';
import { PropertiesListMock } from '../../mock.objects/property.list.mock';
import { PropertyService } from '../../services/property.service';

@Component({
  selector: 'ng-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

    public isLoggedIn: boolean = false;

    public homePropertyTypeOptionMap = new Map(HomePropertyTypeOptionMap);
    public recentHomeProperties: Home[];
    
    constructor(
      private authServe: AuthorizationService,
      private propertyService: PropertyService
    ) { }

    ngOnInit( ) {
      let user = this.authServe.getLoggedInUser();
      if(!jScript.isUndefined(user))  {
        this.isLoggedIn = true;
      }
      this.getPopularListings();
      this.recentProperties();
    }

    public popularList: HomeListing[];

    public getPopularListings() :void  {
      this.popularList = PropertiesListMock.getPopulatHomesList();
    }

    private recentProperties()  {
      this.propertyService.recentProperties().subscribe((properties: any ) => {
        this.recentHomeProperties = properties.homes;
      });
    }
}