import { Component, OnInit } from '@angular/core';

import { AuthorizationService } from '../../services/authorization.service';
import { PropertyType } from '../../models/common/base.types';
import { User } from '../../models/entities/user.model';


@Component({
  selector: 'ng-post-property',
  templateUrl: './post-property.component.html',
  styleUrls: ['./post-property.component.css']
})
export class PostPropertyComponent implements OnInit {
  
  private user: User;

  public propertyType: String;

  public PROPERTY_TYPE = PropertyType;

  public selection: String;
  
  constructor(private authService: AuthorizationService) { }

  ngOnInit() {
    this.user = this.authService.getLoggedInUser();
  }
}
