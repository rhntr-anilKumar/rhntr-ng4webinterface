import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { environment } from '../../../../environments/environment';
import { FilesItem } from '../../../models/entities/files.item';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { UploadFileService } from '../../../services/upload.file.service';


@Component({
  selector: 'app-photo-box',
  templateUrl: './photo-box.component.html',
  styleUrls: ['./photo-box.component.css']
})
export class PhotoBoxComponent implements OnInit {

  private selectedFiles: FileList;
  private currentUploadingFile: File;

  public imagesList: FilesItem[] = new Array();

  @Output()
  public onPhohtoUpload = new EventEmitter<any>();

  constructor(private uploadService: UploadFileService) { }

  ngOnInit() {  }

  public fileSelected(event)  {
    this.selectedFiles = event.target.files;
    this.upload();
  }

  private upload() {
    
    this.currentUploadingFile = this.selectedFiles.item(0);

    if(this.currentUploadingFile.type.split('/')[0] != 'image') return;
    
    this.uploadService.uploadFileToServer(this.currentUploadingFile)
    .subscribe(event => {
      if(event.type === HttpEventType.UploadProgress) {
        console.log('uploading ... ' + Math.round(100 * event.loaded / event.total));
      }
      if(event instanceof HttpResponse) {
        if(event.status == 200 && event.statusText == 'OK') {
          let body: any = event.body;
          let item: FilesItem = JSON.parse(body);
          item.originalFileName = this.currentUploadingFile.name;
          item.fileDownloadUri = environment.IMAGE_RESOURCE_URL + item.fileName;
          this.imagesList.push(item);
          console.log(JSON.stringify(this.imagesList));
          this.notifyPhotoAddEvent();
        }
      }
    });
  }

  public removeImage(imgIndex)  {
    this.imagesList.splice(imgIndex, 1);
  }

  private notifyPhotoAddEvent() {
    this.onPhohtoUpload.emit(this.imagesList);
  }
}