import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { HttpResponse, HttpEventType } from '@angular/common/http';

import { AddressLocator } from '../../../common/address.locator';
import { AuthorizationService } from '../../../services/authorization.service';
import { errorHandler } from '../../../common/error.handler';
import { ErrorResponse } from '../../../models/common/error.response';
import { FilesItem } from '../../../models/entities/files.item';
import { Home, HomeOptionsList } from '../../../models/entities/home';
import { jScript } from '../../../common/jscript';
import { LogInComponent } from '../../log-in/log-in.component';
import { PostPropertyService } from '../../../services/post.property.service';
import { SearchObjects } from '../../../mock.objects/search.objects';
import { UploadFileService } from '../../../services/upload.file.service';


declare var jQuery: any;

@Component({
  selector: 'ng-property-home',
  templateUrl: './property-home.component.html',
  styleUrls: ['./property-home.component.css']
})
export class PropertyHomeComponent implements OnInit {

  public amenitiesList: any;

  public homeRegForm: FormGroup;

  private homePropertyData: Home;

  private imagesList: FilesItem[] = new Array();

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthorizationService,
    private postService: PostPropertyService
  ) { }

  ngOnInit() {
    this.amenitiesList = HomeOptionsList.amenities;
    this.buildForm();
  }

  private buildForm() {
    this.homeRegForm = this.formBuilder.group({
      name: new FormControl(),
      constructedOn: new FormControl(new Date()),
      street: new FormControl(),
      landMark: new FormControl(),
      address: new FormControl(),

      nonVegAllowed: new FormControl(),
      petsAllowed: new FormControl(),
      parking: new FormControl(),

      tenant: new FormControl(), 
      buildUpArea: new FormControl(),
      bhkLength: new FormControl(),
      furnished: new FormControl(),
      floor: new FormControl(),
      facing: new FormControl(),

      rent: new FormControl(),
      deposit: new FormControl(),
      negotiable: new FormControl(),
      
      depositRefundable: new FormControl(),
      maintainanceCharge: new FormControl(),
      agreemantDuriation: new FormControl(),
      leavingIntimation: new FormControl(),
    });
  }

  public submitPropetyData() {

    this.homePropertyData = this.homeRegForm.value;
    this.homePropertyData.amenities = jScript.getSelectedCheckBoxValues(this.amenitiesList);
    this.homePropertyData.addressSpace = AddressLocator.getLocatedAddress('homeAddressHolder');
    this.homePropertyData.user = this.authService.getLoggedInUser();
    this.homePropertyData.homePictures = this.imagesList;

    errorHandler.hideAllErrors();
    console.log('home property reg  : ' + JSON.stringify(this.homePropertyData));

    if(this.validatePostingProperty())  {

      this.postService.postHomeRegistraionRequest(this.homePropertyData)
      .then((res) => { alert('posted successfully...') })
      .catch((err: ErrorResponse) => {
        alert(JSON.stringify(err));
        errorHandler.populateErrors(err, 'errorMessages');
      });
    } else {
      errorHandler.setErrorMessage('errorMessages', 'please select all maditory fields');
    }
  }

  private validatePostingProperty(): boolean {

    let isValid = true;

    if(this.homePropertyData.homePictures == null 
        || this.homePropertyData.homePictures.length == 0) {
      errorHandler.setErrorMessage('errorMessages', 'please select images for property');
      return;
    }
    if(jScript.isNullOrEmpty(this.homePropertyData.name)){
      console.log('this.homePropertyData.name : ' + this.homePropertyData.name)
      isValid = false;
    }
    if(this.homePropertyData.constructedOn == null) {
      console.log('this.homePropertyData.constructedOn : ' + this.homePropertyData.constructedOn)
      isValid = false;
    }
    if(jScript.isNullOrEmpty(this.homePropertyData.tenant)) {
      console.log('this.homePropertyData.tenant : ' + this.homePropertyData.tenant)
      isValid = false;
    }
    if(this.homePropertyData.bhkLength == null) {
      console.log('this.homePropertyData.bhkLength : ' + this.homePropertyData.bhkLength)
      isValid = false;
    }
    if(this.homePropertyData.deposit == null) {
      console.log('this.homePropertyData.deposit : ' + this.homePropertyData.deposit)
      isValid = false;
    }
    if(this.homePropertyData.rent == null)  {
      console.log('this.homePropertyData.rent : ' + this.homePropertyData.rent)
      isValid = false;
    }
    if(this.homePropertyData.agreemantDuriation == null)  {
      console.log('this.homePropertyData.agreemantDuriation : ' + this.homePropertyData.agreemantDuriation)
      isValid = false;
    }
    if(this.homePropertyData.buildUpArea == null) {
      console.log('this.homePropertyData.buildUpArea : ' + this.homePropertyData.buildUpArea)
      isValid = false;
    }
    if(this.homePropertyData.negotiable == null)  {
      console.log('this.homePropertyData.negotiable : ' + this.homePropertyData.negotiable)
      isValid = false;
    }    
    return isValid;
  }
 
  @ViewChild('wizardDiv') wizardDiv: ElementRef;
  @ViewChild('homeAddressLocator') homeAddressLocator: ElementRef;

  ngAfterViewInit() {
    jQuery(this.wizardDiv.nativeElement).steps({
      headerClassName: 'steps-tabs-header',
      bodyClassName: 'steps-tabs-content',
      transitionEffect: "slideLeft"
    });

    jQuery(this.homeAddressLocator.nativeElement).initAutocomplete('homeAddressHolder');
  }

  public photoUploadListener(event) {
    this.imagesList = event;
  }
}