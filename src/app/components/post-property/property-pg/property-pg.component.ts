import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';

import { AddressLocator } from '../../../common/address.locator';
import { AuthorizationService } from '../../../services/authorization.service';
import { errorHandler } from '../../../common/error.handler';
import { ErrorResponse } from '../../../models/common/error.response';
import { FilesItem } from '../../../models/entities/files.item';
import { jScript } from '../../../common/jscript';
import { PayingGuest } from '../../../models/entities/paying.guest';
import { PgOptionsList } from '../../../models/filters/pg.filter.inputs';
import { PGSpaceProperties } from '../../../models/entities/pg.space.properties';
import { PostPropertyService } from '../../../services/post.property.service';

declare var jQuery: any;

@Component({
  selector: 'ng-property-pg',
  templateUrl: './property-pg.component.html',
  styleUrls: ['./property-pg.component.css']
})
export class PropertyPgComponent implements OnInit, AfterViewInit {

  public sharingOptions: any;
  public avaibleServices: any;
  public amenitiesList: any;

  public pgRegForm: FormGroup;
  private pgRegData: PayingGuest;

  private imagesList: FilesItem[] = new Array();

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthorizationService,
    private postService: PostPropertyService
  ) { }

  ngOnInit() {
    this.sharingOptions = PgOptionsList.sharingOptionsForPostProperty;
    this.avaibleServices = PgOptionsList.availbleServices;
    this.amenitiesList = PgOptionsList.amenitiesList;
    this.buildForm();
  }

  private buildForm() {
    this.pgRegForm = this.formBuilder.group({
      name: new FormControl(),
      type: new FormControl(),
      foodType: new FormControl(),
      street: new FormControl(),
      landMark: new FormControl(),
      address: new FormControl(),
      parking: new FormControl(false),
      advance: new FormControl(),
      refundable: new FormControl(false),
      negotiable: new FormControl(false),
      sharingsTypeList: this.formBuilder.array([])
    });
  }

  private createSharingsType(sharingTypeText, sharingType): FormGroup  {
    return this.formBuilder.group({
      sharingTypeText: new FormControl(sharingTypeText),
      sharingType: new FormControl(sharingType),
      withFoodAmount: new FormControl(),
      withoutFoodAmount: new FormControl()
    });
  }

  public onChangeOfSharing(sharingType: any) {
    let sharingAmountList: FormArray = this.getSharingsTypeList();
    if(sharingType.checked) {
      sharingAmountList.push(this.createSharingsType(sharingType.text, sharingType.value));
    } else {
      sharingAmountList.removeAt(sharingAmountList.value
        .findIndex(type => sharingType.value == type.sharingType));
    }
  }

  private getSharingsTypeList(): FormArray {
    return <FormArray> this.pgRegForm.controls.sharingsTypeList;;
  }

  public submitPgPropetyData() {

    this.pgRegData = this.pgRegForm.value;
    this.pgRegData.spaceProperties = [];
    
    this.pgRegData.amenities = jScript.getSelectedCheckBoxValues(this.amenitiesList);
    this.pgRegData.services = jScript.getSelectedCheckBoxValues(this.avaibleServices);

    this.pgRegData.addressSpace = AddressLocator.getLocatedAddress('pgAddressHolder');
    this.pgRegData.user = this.authService.getLoggedInUser();
    this.pgRegData.pgPictures = this.imagesList;

    let sharingsTypeList: FormArray = this.getSharingsTypeList();
    sharingsTypeList.controls.forEach(sharing =>  {
        let sharingInfo: any = sharing.value;
        let pgProperty: PGSpaceProperties = new PGSpaceProperties();
        pgProperty.sharing = sharingInfo.sharing;
        pgProperty.sharingType = sharingInfo.sharingType;
        pgProperty.monthlyRent = sharingInfo.withFoodAmount;
        this.pgRegData.spaceProperties.push(pgProperty);
      }
    );

    console.log('pg property reg  : ' + JSON.stringify(this.pgRegData));

    errorHandler.hideAllErrors();

    if(this.validatePostingProperty())  {
      console.log('pg property reg : ' + JSON.stringify(this.pgRegData));
      this.postService.postPgRegistraionRequest(this.pgRegData)
      .then((res) => { alert('posted successfully...') })
      .catch((err: ErrorResponse) => {
        errorHandler.populateErrors(err, 'errorMessages');
      });
    } else {
      errorHandler.setErrorMessage('errorMessages', 'please select all maditory fields');
    }
  }

  private validatePostingProperty(): boolean {
    
    let isValid = true;
    
    if(this.pgRegData.pgPictures.length == 0) {
      errorHandler.setErrorMessage('errorMessages', 'please select images for property');
      return;      
    }
    if(jScript.isNullOrEmpty(this.pgRegData.name))  {
      console.log('this.pgRegData.name : ' + this.pgRegData.name)
      isValid = false;
    }
    if(jScript.isNullOrEmpty(this.pgRegData.type)) {
      console.log('this.pgRegData.type : ' + this.pgRegData.type)
      isValid = false;
    }
    if(jScript.isNullOrEmpty(this.pgRegData.foodType)) {
      console.log('this.pgRegData.foodType : ' + this.pgRegData.foodType)
      isValid = false;
    }
    if(this.pgRegData.advance == null) {
      console.log('this.pgRegData.advance : ' + this.pgRegData.advance)
      isValid = false;
    }
    if(this.pgRegData.negotiable == null)  {
      console.log('this.pgRegData.negotiable : ' + this.pgRegData.negotiable)
      isValid = false;
    }    
    if(jScript.isNullOrEmpty(this.pgRegData
      .addressSpace.formattedAddress)) {
        console.log('this.pgRegData.addressSpace.formattedAddress : ' + this.pgRegData.addressSpace.formattedAddress)
        isValid = false;
      }
    if(this.pgRegData.spaceProperties.length == 0)  {
      console.log('this.pgRegData.spaceProperties.length : ' + this.pgRegData.spaceProperties.length)
      isValid = false;
    } 
    return isValid;
  }


  @ViewChild('wizardDiv') wizardDiv: ElementRef;
  @ViewChild('pgAddressLocator') pgAddressLocator: ElementRef;

  ngAfterViewInit() {
    jQuery(this.wizardDiv.nativeElement).steps({
      headerClassName: 'steps-tabs-header',
      bodyClassName: 'steps-tabs-content',
      transitionEffect: "slideLeft"
    });

    jQuery(this.pgAddressLocator.nativeElement).initAutocomplete('pgAddressHolder');
  }

  public photoUploadListener(event) {
    alert('event catched....')
    this.imagesList = event;
  }
}