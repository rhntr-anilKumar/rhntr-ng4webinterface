import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

import { AddressLocator } from '../../../common/address.locator';
import { AuthorizationService } from '../../../services/authorization.service';
import { jScript } from '../../../common/jscript';
import { PostPropertyService } from '../../../services/post.property.service';
import { Apartment, ApartmentOptionsList } from '../../../models/entities/apartment';
import { errorHandler } from '../../../common/error.handler';

declare var jQuery: any;

@Component({
  selector: 'ng-property-apartment',
  templateUrl: './property-apartment.component.html',
  styleUrls: ['./property-apartment.component.css']
})
export class PropertyApartmentComponent implements OnInit, AfterViewInit {

  public amenitiesList: any;

  public apartmentRegForm: FormGroup;

  private apartmentPropertyData: Apartment;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthorizationService,
    private postService: PostPropertyService
  ) { }

  ngOnInit() {
    this.amenitiesList = ApartmentOptionsList.AMENTIIES_LIST;
    this.buildForm();
  }

  private buildForm() {
    this.apartmentRegForm = this.formBuilder.group({
      name: new FormControl(),
      constructedOn: new FormControl(new Date()),
      street: new FormControl(),
      landMark: new FormControl(),
      address: new FormControl(),

      nonVegAllowed: new FormControl(),
      petsAllowed: new FormControl(),
      parking: new FormControl(),

      tenent: new FormControl(), 
      buildUpArea: new FormControl(),
      bhkLength: new FormControl(),
      furnished: new FormControl(),
      floor: new FormControl(),
      facing: new FormControl(),

      rent: new FormControl(),
      deposit: new FormControl(),
      negotiable: new FormControl(),
      
      advanceBookingAmount: new FormControl(),
      depositRefundable: new FormControl(),
      maintainanceCharge: new FormControl(),
      agreemantDuriation: new FormControl(),
      leavingIntimation: new FormControl(),
    });
  }

  public submitPropetyData() {
    
    this.apartmentPropertyData = this.apartmentRegForm.value;
    this.apartmentPropertyData.amenities = jScript.getSelectedCheckBoxValues(this.amenitiesList);
    this.apartmentPropertyData.addressSpace = AddressLocator.getLocatedAddress('apartmentAddressHolder');
    this.apartmentPropertyData.user = this.authService.getLoggedInUser();

    console.log('Apartment Property : ' + JSON.stringify(this.apartmentPropertyData));

    this.postService.postApartmentRegistraionRequest(this.apartmentPropertyData)
    .then((res) => alert('Success' + JSON.stringify(res)))
    .catch((err) => {
      alert(JSON.stringify(err));
      errorHandler.populateErrors(err, 'errorMessages');
    });
  }
 
  @ViewChild('wizardDiv') wizardDiv: ElementRef;
  @ViewChild('apartmentAddressLocator') apartmentAddressLocator: ElementRef;

  ngAfterViewInit() {
    jQuery(this.wizardDiv.nativeElement).steps({
      headerClassName: 'steps-tabs-header',
      bodyClassName: 'steps-tabs-content',
      transitionEffect: "slideLeft"
    });

    jQuery(this.apartmentAddressLocator.nativeElement).initAutocomplete('apartmentAddressHolder');
  }
}