import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyApartmentComponent } from './property-apartment.component';

describe('PropertyApartmentComponent', () => {
  let component: PropertyApartmentComponent;
  let fixture: ComponentFixture<PropertyApartmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyApartmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyApartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
