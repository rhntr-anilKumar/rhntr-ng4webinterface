import { Component, OnInit, Input, AfterViewInit, ViewChild } from '@angular/core';

import { FilesItem } from '../../models/entities/files.item';

import { environment } from '../../../environments/environment';
import * as $ from 'jquery';

@Component({
  selector: 'app-photo-viewer',
  templateUrl: './photo-viewer.component.html',
  styleUrls: ['./photo-viewer.component.css']
})
export class PhotoViewerComponent implements OnInit, AfterViewInit {


  public viewerId = new Date().getTime();
  private parentRef = '#slider-window-' + this.viewerId;

  @Input()
  public imagesList: FilesItem[];

  public currentURL: String;

  public noOfPics: number;
  public currentPicIndex: number;

  constructor(  ) { }

  ngOnInit() {

    if(this.imagesList == null || this.imagesList.length <= 0)  {
      this.currentURL = null;
      this.noOfPics = 0;
    } else {
      this.imagesList.forEach(function(image) {
        image.fileDownloadUri = environment.IMAGE_RESOURCE_URL + image.fileName;
      });
      this.currentURL = this.imagesList[0].fileDownloadUri;
      this.noOfPics = this.imagesList.length - 1;
    }
  }

  ngAfterViewInit() {
    
    let self = this;  // to hold parent controll reference

    $('#display-pic-' + self.viewerId).click(function()  {
      $(self.parentRef).show();
      $(self.parentRef + ' .mySlides').hide();
      $(self.parentRef + ' .mySlides:first-child').show();
      $(self.parentRef + ' .mySlides:first-child').addClass('first active');
      $(self.parentRef + ' .mySlides:last-child').addClass('last');
      $(self.parentRef + ' .prev').hide();
      $(self.parentRef + ' .next').show();
    });

    $(self.parentRef + ' .slide-closer').click(function() {
      $(self.parentRef).hide();
    });

    $(self.parentRef +' .prev').click(function() {
      let activeElement = $(self.parentRef +' .mySlides').siblings('.active');
      self.changeSlideView(activeElement, activeElement.prev());
    });

    $(self.parentRef +' .next').click(function() {
      let activeElement = $(self.parentRef +' .mySlides').siblings('.active');
      self.changeSlideView(activeElement, activeElement.next());
    });
  }

  public selectThis(selectedId) {
    this.changeSlideView($(this.parentRef + ' #' + selectedId).siblings(),
       $(this.parentRef + ' #' + selectedId));
  }

  private changeSlideView(currentElement, newElement)  {

 
    currentElement.removeClass('active');
    currentElement.hide();
    newElement.addClass('active');
    newElement.show();

    $(this.parentRef +' .prev').show();  $(this.parentRef +' .next').show();

    if(newElement.hasClass('first'))  {
      $(this.parentRef +' .prev').hide();
    } else if(newElement.hasClass('last'))  {
      $(this.parentRef +' .next').hide();
    }
  }
}