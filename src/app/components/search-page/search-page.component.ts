import { Component, OnInit } from '@angular/core';

import { Home, HomePropertyTypeOptionMap, FurnishedTypeOptionMap, HomeOptionsList } from '../../models/entities/home';
import { HomePropertyMock } from '../../mock.objects/home.property.mock';
import { jScript } from '../../common/jscript';
import { PayingGuest } from '../../models/entities/paying.guest';
import { PGPropertyMock } from '../../mock.objects/pg.property.mock';
import { PropertyType } from '../../models/common/base.types';
import { SearchInputs } from '../../models/entities/search.inputs.module';
import { SearchService } from '../../services/search.service';
import { RestResponse } from '../../models/common/rest.response';
import { User } from '../../models/entities/user.model';

import * as $ from 'jquery';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'ng-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit {

  private searchInputs: SearchInputs;
  private homeProperties: Home[];
  private pgProperties: PayingGuest[];
  private changedFilterInput: String;
  public propertiesExists: boolean = false;

  private object: any;
  public propertyType: String = null;
  public PROPERTY_TYPE = PropertyType;
  public homePropertyTypeOptionMap = HomePropertyTypeOptionMap;
  public furnishedTypeOptionMap = FurnishedTypeOptionMap;

  /**
   * @param searchService
   *    Search Service for HTTP Service.  
   */
  constructor(private searchService: SearchService) { }

  ngOnInit( ) {
    $('.advance-filter').click(function ( ) {
      var filterId = $(this).attr('target');
      $('#' + filterId).slideToggle('slow');
    });
   }

   /**
    * Listener for catching search inputs from Google Map API inputs. 
    * 
    * @param eventSource
    */
  public onSearchInputChangedListener(eventSource)  {
    this.searchInputs = eventSource;
    this.propertyType = this.searchInputs.propertyType;
    this.processSearchRequest(this.searchInputs);
  }

  /**
   * Http service request for search operation.
   * 
   * @param searchInputs
   */
  private processSearchRequest(searchInputs: SearchInputs)  {

    this.searchService.initialSearch(searchInputs)
    .subscribe((res: RestResponse) => {
      console.log('from response')
      this.processResponse(res);
    },
    (err) => {
      console.log('from dummy response' + JSON.stringify(err))
      if (this.propertyType == PropertyType.HOME) {
        this.processResponse(HomePropertyMock.propertyList);
      } 
      else if (this.propertyType == PropertyType.PG) {
        this.processResponse(PGPropertyMock.propertyList);
      }
    });
  }

  /**
   * Processing response of HTTP search request.  
   * 
   * @param response
   */
  private processResponse(response: RestResponse) {

    console.log('response data : ' + JSON.stringify(response));
    this.searchInputs = response.input;
    this.propertiesExists = response.data.length > 0 ? true : false;
    console.log('property list loaded.[' + response.data.length + ']');
    if(this.propertiesExists)  {
      if (this.propertyType == PropertyType.HOME)  {
        this.processHomeProperties(response.data);
      } else if (this.propertyType ==  PropertyType.PG)  {
        this.processPgProperties(response.data);
      }
    }
  }

  /**
   * Pre-process the resulting list of home properties.
   * Calculating other fields values.
   */
  private processHomeProperties(homePropertyList) {
    this.homeProperties = new Array();
    homePropertyList.forEach(element => {
      let property: Home = element;
      property.user = new User();
      property.user.id = element.addressSpace.userId;
      property.addressSpace = element.addressSpace.googledAddress;
      this.homeProperties.push(property);
    });
  }

  public tenantMappedValue(key)  {
    let tenant = HomeOptionsList.tenant.find(e => e.value == key);
    return tenant == undefined ? '' : tenant.text;
  }

  /**
   * Pre-process the resulting list of pg properties.
   * Calculating other fields values.
   */
  private processPgProperties(pgPropertyList) {

    if(!jScript.isUndefined(pgPropertyList))  {

      this.pgProperties = new Array();
      pgPropertyList.forEach(element => {
        let property: PayingGuest = element;
        property.user = new User();
        property.user.id = element.addressSpace.userId;
        property.addressSpace = element.addressSpace.googledAddress;

        let availbaleSharaings = new Array();
        property.spaceProperties.forEach(sharingType => {
          availbaleSharaings.push(sharingType.sharing);
        });
        property.sharings = availbaleSharaings;

        property.sharing = property.spaceProperties[0].sharing;
        property.sharingType = property.spaceProperties[0].sharingType;
        property.monthlyRent = property.spaceProperties[0].monthlyRent;

        if(property.pgPictures != null && property.pgPictures.length > 0) {
          property.curPicURL = environment.REST_API_BASE_PATH 
            + '/downloadFile/' + property.pgPictures[0].fileName;
        } else {
          property.curPicURL = 'assets/img/rhntr-home.jpg';
        }
          
        this.pgProperties.push(property);
      });
      console.log('After processing : ' + JSON.stringify(this.pgProperties));
    }
  }

  /**
   * Listener to catch the filter input change event 
   * from chid filter components.   
   * 
   * @param changedFilterSource 
   */
  public onFilterInputsChangedListener(changedFilterSource) {
    this.changedFilterInput = JSON.stringify(changedFilterSource);
  }
}