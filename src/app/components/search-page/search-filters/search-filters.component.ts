import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { HomeFilter } from '../../../models/filters/home.filter.inputs';
import { HomeOptionsList } from '../../../models/entities/home';
import { jScript } from '../../../common/jscript';
import { PropertyType } from '../../../models/common/base.types';
import { PgFilter, PgOptionsList } from '../../../models/filters/pg.filter.inputs';

import * as $ from 'jquery';

@Component({
  selector: 'ng-search-filters',
  templateUrl: './search-filters.component.html',
  styleUrls: ['./search-filters.component.css']
})
export class SearchFiltersComponent implements OnInit {

  @Input()
  public propertyType: string;
  public PROPERTY_TYPE = PropertyType;

  // Home related properties
  public homeFilter: HomeFilter;
  public bhkLengthOptionList;
  public furnishedOptionList;
  public tenantOptionList;
  public floorsOptionList;
  public amenitiesOptionList;
  public parkingOptionList;

  // Pg related properties.
  public pgFilter: PgFilter;
  public pgTypeOptions;
  public sharingOptions;


  @Output()
  public onFilterInputsChanged = new EventEmitter<any>();

  constructor(  ) { }

  public ngOnInit() {
    /* Jquery specific to page */
    $(document).ready(function( )  {
      $('.advance-filter').click(function ( ) {
        var filterId = $(this).attr('target');
        $('#' + filterId).slideToggle('slow');
        $('#' + filterId + '-advance-panel').fadeToggle('slow');
      });
    });
    
    if(this.propertyType == PropertyType.HOME)  {
      this.bindHomeFilterProperties();
    }
    else if(this.propertyType == PropertyType.PG)  {
      this.bindPgFilterProperties();
    }
  }


  /**
   * Home property related form setting and data population.
   */
  private bindHomeFilterProperties()  {
    this.homeFilter = new HomeFilter();
    this.tenantOptionList = HomeOptionsList.tenant;
    this.bhkLengthOptionList = HomeOptionsList.bhkLength;
    this.furnishedOptionList = HomeOptionsList.furnished;
    this.floorsOptionList = HomeOptionsList.floors;
    this.amenitiesOptionList = HomeOptionsList.amenities;
    this.parkingOptionList = HomeOptionsList.parking;
  }

  /**
   * Triggers event when input changes in home filter form.
   */
  public onHomeFilterChange() {
    this.homeFilter.tenant = jScript.getSelectedCheckBoxValues(this.tenantOptionList);
    this.homeFilter.bhkLength = jScript.getSelectedCheckBoxValues(this.bhkLengthOptionList);
    this.homeFilter.furnished = jScript.getSelectedCheckBoxValues(this.furnishedOptionList);
    this.homeFilter.floor = jScript.getSelectedCheckBoxValues(this.floorsOptionList);
    this.homeFilter.amenities = jScript.getSelectedCheckBoxValues(this.amenitiesOptionList);
    this.homeFilter.parking = jScript.getSelectedCheckBoxValues(this.parkingOptionList);
    this.notifyFilterChangedEvent(this.homeFilter);
  }

  /**
   * PG property related form setting and data population.
   */
  public bindPgFilterProperties() {
    this.pgFilter = new PgFilter();
    this.pgTypeOptions = PgOptionsList.pgTypes;
    this.sharingOptions = PgOptionsList.sharings;
  }

  /**
   * Handling on inputs event/trigger changes for any element within form.
   */
  public onPgFilterChange()  {
    this.pgFilter.pgType = jScript.getSelectedCheckBoxValues(this.pgTypeOptions);
    this.pgFilter.sharings = jScript.getSelectedCheckBoxValues(this.sharingOptions);
    this.notifyFilterChangedEvent(this.pgFilter);
  }


  /**
   * restting filter inputs to default values
   */
  public resetFilter()  {
  
    if(this.propertyType == PropertyType.HOME)  {
      this.resetHomeFilter();
    } else if(this.propertyType == PropertyType.PG)  {
      this.resetPgFilter();
    }
  }

  /**
   * Home filter reset actions
   */
  private resetHomeFilter() {

    this.homeFilter = new HomeFilter();
    jScript.resetAll(this.tenantOptionList);
    jScript.resetAll(this.bhkLengthOptionList);
    jScript.resetAll(this.furnishedOptionList);
    jScript.resetAll(this.floorsOptionList);
    jScript.resetAll(this.amenitiesOptionList);
    jScript.resetAll(this.parkingOptionList);
    this.notifyFilterChangedEvent(this.homeFilter);
  }

  /**
   * Pg filter reset actions
   */
  private resetPgFilter() {
    this.pgFilter = new PgFilter();
    jScript.resetAll(this.pgTypeOptions);
    jScript.resetAll(this.sharingOptions);
    this.notifyFilterChangedEvent(this.pgFilter);
  }
  

  /**
   * Posting/Notifying change event of search filter inputs.   
   * 
   * @param filterData
   */
  private notifyFilterChangedEvent(filterData: any)  {
    this.onFilterInputsChanged.emit(filterData);
  }
}