import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePropertyViewComponent } from './home-property-view.component';

describe('HomePropertyViewComponent', () => {
  let component: HomePropertyViewComponent;
  let fixture: ComponentFixture<HomePropertyViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePropertyViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePropertyViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
