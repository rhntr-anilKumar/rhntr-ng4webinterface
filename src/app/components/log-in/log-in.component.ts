import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { AuthorizationService } from '../../services/authorization.service';
import { errorHandler } from '../../common/error.handler';
import { ErrorResponse } from '../../models/common/error.response';
import { jScript } from '../../common/jscript';
import { User } from '../../models/entities/user.model';
import { UsersMock } from '../../mock.objects/user.mock';

import * as $ from 'jquery';

@Component({
  selector: 'ng-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

    public passcode: String;
    public emailOrPhoneNo: String;

    @Input()
    public isLoggedIn: boolean;
  
    @Output()
    public isLoggedInChange = new EventEmitter<boolean>();
    private setIsLoggedIn(isLoogedIn:boolean) {
        this.isLoggedIn = isLoogedIn;
        this.isLoggedInChange.emit(this.isLoggedIn);
    }

    public user: User;
    public cpassword: String;

    constructor(private authService: AuthorizationService)   {   }

    ngOnInit() {
      
      this.user = this.authService.getLoggedInUser();
      if(this.user == null) {
        this.setIsLoggedIn(false)
        this.user = new User()
      } else {
        this.setIsLoggedIn(true)
      }

      $('#confirmPasscode, #passcode').blur(function() {
        if($('#confirmPasscode').val() != $('#passcode').val())
          $('#cpCompare').show()
        else
          $('#cpCompare').hide()
      });
    }

    /* log out not working fine please refresh for testing */
    public signOut() {
      this.setIsLoggedIn(false)
      this.authService.signOutCurrentUser()
    }

    public processLogInRequest()   {
        
        this.user = new User()
        this.user.passcode = this.passcode
        if(this.emailOrPhoneNo.match('^[A-Za-z0-9+_.-]+@(.+)$'))    {
            this.user.emailId = this.emailOrPhoneNo
        }else   {
            this.user.phoneNumber = this.emailOrPhoneNo
        }

        this.authService.authenticateUser(this.user)
        .then((res: User) =>{
          this.setLoggedInUserInScope(res);
        })
        .catch((err) => {
          alert('Error on log in : ' + JSON.stringify(err));
          $('#emailOrPhoneNoError').show();
          //this.setLoggedInUserInScope(UsersMock.loggedInUser);
        });
    }

    public setLoggedInUserInScope(user) {
      this.user = user;
      this.authService.setLoggedInUser(user);
      this.setIsLoggedIn(true);
      this.closeLogInPopUpWindow();
    }

    public resetSignUpForm()    {
        this.user = new User()
    }

    public registerUser()   {
      errorHandler.hideAllErrors();
      if(this.user.confirmPasscode  == this.user.passcode)    {
        this.authService.registerNewUser(this.user)
        .then((registeredUser) => {
          this.closeLogInPopUpWindow()
        }).catch((err: ErrorResponse) => {
          errorHandler.populateErrors(err, 'errMessages');
        });
      }
    }

    public openSignInWindow() {
        $('#log-in-popup').fadeIn(300)
    }

    public closeLogInPopUpWindow() {
        $('#log-in-popup').fadeOut(300)
    }
}