import { Component, OnInit, Input, EventEmitter, 
  Output, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { AddressLocator } from '../../common/address.locator';
import { PropertyObjects } from '../../mock.objects/property.mock.object';
import { PropertyType } from '../../models/common/base.types';
import { SearchInputs } from '../../models/entities/search.inputs.module';
import { SearchObjects } from '../../mock.objects/search.objects';
import { SearchService } from '../../services/search.service';

import * as $ from 'jquery';

declare const initAutocomplete: any;
declare var jQuery: any;

@Component({
  selector: 'ng-search-engine',
  templateUrl: './search-engine.component.html',
  styleUrls: ['./search-engine.component.css']
})
export class SearchEngineComponent implements OnInit, AfterViewInit  {

    public propertyType: String;
    public PROPERTY_TYPE = PropertyType;
    public formattedAddress: String;

    public searchInputs: SearchInputs;

    @Input()
    public isHomePage: boolean = false;

    @Output()
    public onSearchInputsChanged = new EventEmitter<any>();

    /**
     * @param searchService 
     *  Search request related services and HTTP support. 
     * @param router 
     *  Routing management and navigation support.
     */
    constructor(
        private searchService: SearchService,
        private router: Router
    ) { }

    ngOnInit( ) {
      let self = this;
      $(document).ready(function(){
        if(self.isHomePage)
          initAutocomplete('addressHolder1')
        else 
          initAutocomplete('addressHolder2')
      });

      if(this.searchService.isHomePageSearchEventTriggered()) {
          this.searchInputs = this.searchService.getSearchInputs();
          this.formattedAddress = this.searchInputs.formattedAddress;
          this.mapSearchInputsToJQuerySelectors(this.searchInputs);
          this.onSearchInputsChanged.emit(this.searchInputs);
      } else {
        this.propertyType = PropertyType.HOME;
      }
    }


    /**
     * Setting back Googled search results to hidden fields. 
     * 
     * @param searchInputs 
     */
    private mapSearchInputsToJQuerySelectors(searchInputs: SearchInputs)  {

        if(this.isHomePage)
            $('#addressHolder1').val(searchInputs.formattedAddress.toString());
        else 
            $('#addressHolder2').val(searchInputs.formattedAddress.toString());
        
        $('#route').val(searchInputs.route.toString());
        $('#sublocality_level_2').val(searchInputs.sublocalityLevel2.toString());
        $('#sublocality_level_1').val(searchInputs.sublocalityLevel1.toString());
        $('#locality').val(searchInputs.locality.toString());
        $('#administrative_area_level_2').val(searchInputs.administrativeAreaLevel2.toString());
        $('#administrative_area_level_1').val(searchInputs.administrativeAreaLevel1.toString());
        $('#country').val(searchInputs.country.toString());
        $('#postal_code').val(searchInputs.postalCode.toString());

        $('#latitude').val(searchInputs.latitude.toString());
        $('#longitude').val(searchInputs.longitude.toString());
        $('#placeId').val(searchInputs.placeId.toString());
        this.propertyType = searchInputs.propertyType;
    }
    
    /**
     * Fetching Googled search results from hidden fields using JQuery selector.
     */
    private collectSearchInputsFromJQuerySelector()  {
      let jQinputs: SearchInputs = null;
      
      if(this.isHomePage)
        jQinputs = AddressLocator.collectSearchInputsFromJQuerySelector('addressHolder1');
      else
        jQinputs = AddressLocator.collectSearchInputsFromJQuerySelector('addressHolder2');

      jQinputs.propertyType = this.propertyType;
      return jQinputs;
    }

    /**
     * Search request for new searcg Googled address.
     * 
     * Redirecting the search event to search result page. 
     * After redirection firing search request throw search service.
     */
    public redirectSearchRequest( ) {

      this.searchInputs = this.collectSearchInputsFromJQuerySelector();
	  //SearchObjects.searchInputs(this.propertyType);
      this.searchService.setSearchInputs(this.searchInputs);
      this.formattedAddress = this.searchInputs.formattedAddress;
      this.searchService.setHomePageSearchEventTriggered();
      this.router.navigate(['/search-page']);
    }

    /**
     * Search request for new searcg Googled address.
     */
    public searchRequest()  {
        this.searchInputs = this.collectSearchInputsFromJQuerySelector();
        //SearchObjects.searchInputs(this.propertyType);
        this.onSearchInputsChanged.emit(this.searchInputs);
    }

    /**
     * Temporary operation for saving some Googled search addresess.
     */
    public saveThisArea()   {
        const address: any =  this.collectSearchInputsFromJQuerySelector();
        address.userId = 3;
        this.searchService.saveSearchAsArea(address)
        .then(
            () => {
                alert('added');
            }
        )
        .catch(
            (err) => {
                alert('Err while adding.');
            }
        );
    }

    ngAfterViewInit() {
      $('.search-tab li').click(function() {
        $(this).siblings().find('a').removeClass('active');
        $(this).find('a').addClass('active');
      });
    }
}