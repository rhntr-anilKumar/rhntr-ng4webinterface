import { Routes } from '@angular/router'
import { AppComponent } from '../app.component'
import { HomePageComponent } from '../components/home-page/home-page.component'
import { SearchPageComponent } from '../components/search-page/search-page.component';
import { TestComponent } from '../components/test/test.component';
import { ProfileComponent } from '../components/profile/profile.component';
import { PostPropertyComponent } from '../components/post-property/post-property.component';
import { AboutUsComponent } from '../components/static/about-us/about-us.component';
import { HomePropertyViewComponent } from '../components/property-viewer/home-property-view/home-property-view.component';


export const AppRoutingMap: Routes = [
  { 
    path: '',
    component : HomePageComponent,
    pathMatch: 'full'
  },
  {
    path: 'search-page',
    component : SearchPageComponent
  },
  { 
    path: 'template',
    component : TestComponent
  },
  {
    path: 'post-property',
    component: PostPropertyComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'property/home/:id',
    component: HomePropertyViewComponent
  },
  {
    path: 'about-us',
    component: AboutUsComponent
  }
];