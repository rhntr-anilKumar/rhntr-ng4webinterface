import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['../assets/css/custom-css.css','./app.component.css']
})

export class AppComponent {
  title = 'app';
}