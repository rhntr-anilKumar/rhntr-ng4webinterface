import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { jScript } from '../common/jscript';
import { SearchService } from './search.service';
import { Subscriber } from 'rxjs';
import { User } from '../models/entities/user.model';

@Injectable()
export class AuthorizationService   {

    private loggedInUser: User;
    private isUserLoggedInFlage: Boolean;
    private LOG_IN_USER_TOKEN: string = 'LOG_IN_USER_TOKEN';
    
    private USER_REST_URI:string;
    private header: Headers = new Headers({'Content-Type': 'application/json'});

    constructor(private httpClient: HttpClient) {
      this.USER_REST_URI = environment.REST_API_BASE_PATH + '/user';
    }

    public authenticateUser(user: User): Promise<any>   {
      return this.httpClient.post(this.USER_REST_URI + '/authenticate', user).toPromise();
    }

    public registerNewUser(user: User): Promise<any> {
      return this.httpClient.post(this.USER_REST_URI, user).toPromise();
    }

    public getLoggedInUser() :User {
      let user :User = JSON.parse(sessionStorage.getItem(this.LOG_IN_USER_TOKEN));
      if(!jScript.isUndefined(user) && !jScript.isUndefined(user.id))  {
        this.loggedInUser = user;
      } else {
        this.loggedInUser = null;
      }
      return this.loggedInUser;
    }

    public setLoggedInUser(user: User)  {
      this.isUserLoggedInFlage = true;
      sessionStorage.setItem(this.LOG_IN_USER_TOKEN, JSON.stringify(user));
      this.loggedInUser = user;
    }

    public signOutCurrentUser() {
      this.isUserLoggedInFlage = false;
      sessionStorage.removeItem(this.LOG_IN_USER_TOKEN)
    }

    public isUserLoggedIn(): Boolean {
      return this.isUserLoggedInFlage;
    }
}