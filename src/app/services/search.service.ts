import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { SearchInputs } from '../models/entities/search.inputs.module';

@Injectable()
export class SearchService {

    private SEARCH_REST_URI:string;

    constructor(private httpClient: HttpClient) {
        this.SEARCH_REST_URI = environment.REST_API_BASE_PATH + '/search';
    }

    private searchInputs: SearchInputs;
    public setSearchInputs(searchInputs: SearchInputs)    {
        this.searchInputs = searchInputs;
    }
    public getSearchInputs(): SearchInputs    {
        return this.searchInputs;
    }

    private homePageSearchTriggered: boolean = false;
    public setHomePageSearchEventTriggered()    {
        this.homePageSearchTriggered = true;
    }
    public isHomePageSearchEventTriggered() {
        const flag: boolean = this.homePageSearchTriggered;
        this.homePageSearchTriggered = false;
        return flag;
    }

    public initialSearch(searchInputs: SearchInputs) {
      let url = this.SEARCH_REST_URI + '/q';
      //return $.post(url, JSON.stringify(searchInputs));
      return this.httpClient.post(url, searchInputs);
    }

    public saveSearchAsArea(address: any):  Promise<any>   {
        return this.httpClient.post(environment.REST_API_BASE_PATH + '/address/', address)
        .toPromise();
    }
}