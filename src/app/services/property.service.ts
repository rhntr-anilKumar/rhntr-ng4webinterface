import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Subscriber } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  private REST_URI: String;

  constructor(private httpClient: HttpClient) {
    this.REST_URI = environment.REST_API_BASE_PATH;
  }

  public recentProperties() {
    return this.httpClient.get(this.REST_URI + '/load/recent-properties');
  }
}
