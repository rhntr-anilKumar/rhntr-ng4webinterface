import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { Apartment } from '../models/entities/apartment';
import { Home } from '../models/entities/home';
import { PayingGuest } from '../models/entities/paying.guest';



@Injectable()
export class PostPropertyService {

  private POSTING_URI:string;

  constructor(private httpClient: HttpClient) {
    this.POSTING_URI = environment.REST_API_BASE_PATH;
  }

  public postPgRegistraionRequest(pgData: PayingGuest): Promise<any> {
    return this.httpClient.post(this.POSTING_URI + '/paying-guest/', pgData).toPromise();
  }

  public postHomeRegistraionRequest(homeData: Home) {
    return this.httpClient.post(this.POSTING_URI + '/home/', homeData).toPromise();
  }

  public postApartmentRegistraionRequest(apartmentData: Apartment) {
    return this.httpClient.post(this.POSTING_URI + '/apartment/', apartmentData).toPromise();
  }
}