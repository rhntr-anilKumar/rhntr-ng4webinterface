import { TestBed, inject } from '@angular/core/testing';
import { PostPropertyService } from './post.property.service';


describe('PostPropertyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PostPropertyService]
    });
  });

  it('should be created', inject([PostPropertyService], (service: PostPropertyService) => {
    expect(service).toBeTruthy();
  }));
});
