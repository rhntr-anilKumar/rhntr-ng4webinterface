import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {

  constructor(private httpClient: HttpClient) { }

  public uploadFileToServer(file: File) : Observable<HttpEvent<{}>>  {

    const formData: FormData = new FormData();
    formData.append('file', file);

    const req = new HttpRequest('POST', environment.REST_API_BASE_PATH + '/uploadFile', formData,  {
      reportProgress: true,
      responseType: 'text'
    });
    return this.httpClient.request(req);
  }
}
