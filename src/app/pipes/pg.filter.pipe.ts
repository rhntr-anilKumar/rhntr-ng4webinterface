import { Pipe, PipeTransform } from '@angular/core';

import { jScript } from '../common/jscript';
import { PayingGuest } from '../models/entities/paying.guest';
import { PgFilter } from '../models/filters/pg.filter.inputs';


@Pipe({
  name: 'pgFilter'
})
export class PgFilterPipe implements PipeTransform {

  transform(pgList: PayingGuest[], filterInput: any): any {

    if(!filterInput)  return pgList;
    if(!pgList)       return null;

    let pgFilter: PgFilter = JSON.parse(filterInput);

    let isPgTypeSelected = !jScript.isArrayNullOrEmpty(pgFilter.pgType);
    let isSharingsSelected = !jScript.isArrayNullOrEmpty(pgFilter.sharings);
    let bothFoodTypeSelected = pgFilter.withFood && pgFilter.withoutFood;

    return pgList.filter(pgItem => {
    
      let isValidEntry = true;

      if(isPgTypeSelected)  {
        isValidEntry = pgFilter.pgType.includes(pgItem.type.toString());
      }

      if(isValidEntry && isSharingsSelected)  {
        isValidEntry = jScript.isArrayContainsAny(pgItem.sharings, pgFilter.sharings);
      }

      if(isValidEntry && !bothFoodTypeSelected)  {
        if(pgFilter.withFood)
          isValidEntry = pgItem.foodType == 'WITH_FOOD';
        if(pgFilter.withoutFood)
          isValidEntry = pgItem.foodType == 'WITHOUT_FOOD';
      }

      if(isValidEntry && pgFilter.minRate && pgFilter.maxRate && pgFilter.minRate < pgFilter.maxRate)  {
        /* check for any of sharing withing range of custom select amount */
        let withingRange = false;
        for(let i = 0; i < pgItem.spaceProperties.length; i++ ) {

          /*  if sharing is selected means, 
              nedd to check only for selected sharing options,
              otherwise check for all type of sharing price */
          let checkForThisSharingIfSelected = true;
          if(isSharingsSelected && !jScript.isArrayContains(pgFilter.sharings, pgItem.spaceProperties[i].sharing))  {
            checkForThisSharingIfSelected = false;
          }

          if(checkForThisSharingIfSelected) {
            if(pgItem.spaceProperties[i].monthlyRent >= pgFilter.minRate 
              && pgItem.spaceProperties[i].monthlyRent <= pgFilter.maxRate) {
              withingRange = true;
              break;
            }
          }
        }
        isValidEntry = withingRange;
      }

      return isValidEntry;
    });
  }
}
