import { Pipe, PipeTransform } from '@angular/core';

import { Home } from '../models/entities/home';
import { HomeFilter } from '../models/filters/home.filter.inputs';
import { jScript } from '../common/jscript';

@Pipe({
  name: 'homeFilter'
})
export class HomeFilterPipe implements PipeTransform {

  transform(homeList: Home[], filterInput: any): any {

    if(!filterInput)  return homeList;
    if(!homeList)       return null;

    let homeFilter: HomeFilter = JSON.parse(filterInput);
  
    let isTenantsSelected = !jScript.isArrayNullOrEmpty(homeFilter.tenant);
    let isBhkLengthSelected = !jScript.isArrayNullOrEmpty(homeFilter.bhkLength);
    let isFurishedSelected = !jScript.isArrayNullOrEmpty(homeFilter.furnished);
    let isFloorsSelected = !jScript.isArrayNullOrEmpty(homeFilter.floor);
    let isAmenitiesSelected = !jScript.isArrayNullOrEmpty(homeFilter.amenities);
    let isParkingSelected = !jScript.isArrayNullOrEmpty(homeFilter.parking);

    return homeList.filter(homeItem => {
      
      let isValidEntry = true;

      if(isTenantsSelected) {
        isValidEntry = jScript.isArrayContains(homeFilter.tenant, homeItem.tenant);
      }

      if(isValidEntry && isBhkLengthSelected) {
        isValidEntry = jScript.isArrayContains(homeFilter.bhkLength, homeItem.spaceProperties.bhkLength)
      }

      if(isValidEntry && isFurishedSelected)  {
        isValidEntry = jScript.isArrayContains(homeFilter.furnished, homeItem.spaceProperties.furnished);
      }

      if(isValidEntry && isFloorsSelected)  {
        isValidEntry = jScript.isArrayContains(homeFilter.floor, homeItem.spaceProperties.floor);
      }

      if(isValidEntry && homeFilter.minRate && homeFilter.maxRate 
          && homeFilter.minRate < homeFilter.maxRate)  {

          if(homeItem.spaceProperties.rent < homeFilter.minRate
              || homeItem.spaceProperties.rent > homeFilter.maxRate)  {
              isValidEntry = false;
          }
      }

      return isValidEntry;
    });
  }

}
