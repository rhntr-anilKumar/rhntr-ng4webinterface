import { Injectable } from '@angular/core';

@Injectable()
export class TokenStorage {

  public static TOKEN_KEY = 'AuthToken';
  public static TOKEN_HEADER_KEY = 'Authorization';


  constructor() { }

  signOut() {
    window.sessionStorage.removeItem(TokenStorage.TOKEN_KEY);
    window.sessionStorage.clear();
  }

  public saveToken(token: string) {
    window.sessionStorage.removeItem(TokenStorage.TOKEN_KEY);
    window.sessionStorage.setItem(TokenStorage.TOKEN_KEY,  token);
  }

  public getToken(): string {
    return sessionStorage.getItem(TokenStorage.TOKEN_KEY);
  }
}
