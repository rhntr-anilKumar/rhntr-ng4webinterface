/**
 * jScript
 */
export class jScript   {

  /**
   * @param obj 
   * 
   * check for undefined or null refence objects
   */
  public static isUndefined(obj: any) :boolean {
    return obj == null || obj == undefined;
  }

  /**
   * @param str 
   * 
   * check for undefined, null or empty string.
   */
  public static isNullOrEmpty(str: String) :boolean {
    return jScript.isUndefined(str) || str.trim().length == 0 || str == 'null';
  }

  /**
   * @param array 
   * 
   * check null or empty array.
   */
  public static isArrayNullOrEmpty(array: any) {
    return jScript.isUndefined(array) || array.length == 0;
  }

  /**
   * @param checkBoxList 
   * Evaluating check box array and fetching only checked element.
   */
  public static getSelectedCheckBoxValues(checkBoxList) {
    return checkBoxList.filter(f => f && f.value != null && f.checked).map(type => type.value);
  }

  /**
   * @param checkBoxList 
   * Resetting all selected fields to false
   */
  public static resetAll(checkBoxList) {
    return checkBoxList.forEach(element => element.checked = false)
  }

  /**
   * @param containerArray 
   * @param inputArray 
   * 
   * Performing array search operation for inputsArray in containerArray.
   * To check, if at least one element is exist of inputArray in containerArray.
   */
  public static isArrayContainsAny(containerArray, inputArray) {
    let contains = false;
    for(let i = 0; i < inputArray.length; i++) {
      if(containerArray.some(e => e == inputArray[i]))  {
        contains = true;
        break;
      }
    }
    return contains;
  }

  /**
   * @param containerArray 
   * @param inputArray 
   * 
   * Performing array search operation for inputsArray in containerArray.
   * To check for all element is exist of inputArray in containerArray.
   */
  public static isArrayContainsAll(containerArray, inputArray) {
    let contains = true;
    for(let i =0; i < inputArray.length; i++) {
      if(!containerArray.contains(inputArray[i]))  {
        contains = false;
        break;
      }
    }
    return contains;
  }


  /** 
   * @param containerArray 
   * @param inputVal 
   * 
   * Performing array search operation for inputs value in containerArray.
   */
  public static isArrayContains(containerArray, inputVal) {
    return containerArray.some(e => e == inputVal);
  }

}