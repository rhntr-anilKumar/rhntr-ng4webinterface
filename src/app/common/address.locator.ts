import { AddressSpace } from '../models/entities/address.space';
import { SearchInputs } from '../models/entities/search.inputs.module';
import * as $ from 'jquery';

declare var google: any;

/**
 * AddressLocator handles the google address related 
 * mapping and fetching operations of location information. 
 */
export class AddressLocator   {

  public static collectSearchInputsFromJQuerySelector(addressHolderId): SearchInputs {

    const jQinputs: SearchInputs = new SearchInputs();

    jQinputs.formattedAddress = $('#' + addressHolderId).val().toString();
    jQinputs.streetNumber = $('#street_number').val().toString();
    jQinputs.route = $('#route').val().toString();
    jQinputs.sublocalityLevel2 = $('#sublocality_level_2').val().toString();
    jQinputs.sublocalityLevel1 = $('#sublocality_level_1').val().toString();
    jQinputs.locality = $('#locality').val().toString();
    jQinputs.administrativeAreaLevel2 = $('#administrative_area_level_2').val().toString();
    jQinputs.administrativeAreaLevel1 = $('#administrative_area_level_1').val().toString();
    jQinputs.country = $('#country').val().toString();
    jQinputs.postalCode = $('#postal_code').val().toString();

    jQinputs.latitude = $('#latitude').val().toString();
    jQinputs.longitude = $('#longitude').val().toString();
    jQinputs.placeId = $('#placeId').val().toString();

    return jQinputs;
  }

  public static getLocatedAddress(addressHolderId): AddressSpace {
    
    let addressSpace: AddressSpace = new AddressSpace();

    addressSpace.formattedAddress = $('#' + addressHolderId).val().toString();
    addressSpace.streetNumber = $('#street_number').val().toString();
    addressSpace.route = $('#route').val().toString();
    addressSpace.sublocalityLevel2 = $('#sublocality_level_2').val().toString();
    addressSpace.sublocalityLevel1 = $('#sublocality_level_1').val().toString();
    addressSpace.locality = $('#locality').val().toString();
    addressSpace.administrativeAreaLevel2 = $('#administrative_area_level_2').val().toString();
    addressSpace.administrativeAreaLevel1 = $('#administrative_area_level_1').val().toString();
    addressSpace.country = $('#country').val().toString();
    addressSpace.postalCode = $('#postal_code').val().toString();

    addressSpace.latitude = $('#latitude').val().toString();
    addressSpace.longitude = $('#longitude').val().toString();
    addressSpace.placeId = $('#placeId').val().toString();

    return addressSpace;
  }

  public static getCurrentLoaction()  {
    alert('getCurrentLoaction')
    if (navigator.geolocation)  {
      navigator.geolocation.getCurrentPosition(this.showPosition, this.showError);
    }
  }

  private static showError() {

  }

  private static showPosition(position): String{
    let lat=position.coords.latitude;
    let lon=position.coords.longitude;
    return this.displayLocation(lat,lon);
  }

  private static displayLocation(latitude,longitude)  {
    var place = '';    
    var geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(latitude, longitude);

    geocoder.geocode(
        {'latLng': latlng}, 
        function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var add= results[0].formatted_address ;
                    var  value=add.split(",");

                    let count=value.length;
                    let country=value[count-1];
                    let state=value[count-2];
                    let city=value[count-3];
                    place = results[0].formatted_address + " = city name is: " + value[count-4] + " " +city;
                }
                else  {
                  place = "address not found";
                }
            }
            else {
              place = "Geocoder failed due to: " + status;
            }
        }
    );
    alert('place : ' + place)
    return place;
  }
}