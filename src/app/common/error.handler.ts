import { ErrorResponse } from '../models/common/error.response';
import * as $ from 'jquery';

export class errorHandler   {

  public static hideAllErrors() {
    $('.errors').slideUp("slow");
  }

  public static setErrorMessage(property, message) {
    if($('#' + property) == null) {
      if($('#' + property + 'Error') != null) {
        $('#' + property + 'Error').html(message);
        $('#' + property + 'Error').fadeIn();
      }
    } else {
      $('#' + property).html(message);
      $('#' + property).fadeIn();
    }
  }

  public static populateErrors(err: ErrorResponse, genericMessages: String) {

    if(err.error.errors instanceof Object)  {
      
      if(err.error.errors != null)  {
        err.error.errors.forEach(function(error) {
          $('#' + error.field + 'Error').html(error.message);
          $('#' + error.field + 'Error').slideDown("slow");
        });  
      }

      if(err.error.messages != null)    {
        let message = '';
        err.error.messages.filter(f => f != null && f != '')
        .forEach(msg => {
          message += msg + '<br/>';
        });
        $('#' + genericMessages).html(message);
        $('#' + genericMessages).show();
      }

    } else {
      $('#' + genericMessages).html(err.error.errors);
      $('#' + genericMessages).slideDown();
    }
  }
}